<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::domain('admin.localhost')->group(function () {
Route::domain('admin.rahacares.com')->group(function () {
	
	Route::view('login','auth.login_admin')->name('admin.login');
	Route::post('login','Admin\AdminController@authenticateAdmin');
	

	Route::middleware(['auth_admin'])->group(function () {
	Route::get('logout','Admin\AdminController@logout');
	Route::get('/','Admin\DashboardController@getDashboard')->name('admin.dashboard');
	
	Route::get('orders','Admin\OrderController@getOrders')->name('admin.orders')->middleware('auth_admin');

	Route::get('order/{order_id}','Admin\OrderController@getOrder')->name('admin.order');
	Route::post('dispatchItems','Admin\OrderController@dispatchOrderItems');

	Route::get('transactions','Admin\TransactionController@getTransactions')->name('admin.transactions');

	Route::get('transaction/{transaction_id}','Admin\TransactionController@getTransaction')->name('admin.transaction');

	Route::get('shippings', 'Admin\ShippingController@getShippings')->name('admin.shippings');

	Route::get('shipping/{shipping_id}','Admin\ShippingController@getShipping')->name('admin.shipping');


	Route::get('newShipping','Admin\ShippingController@newShipping')->name('admin.newShipping');
	Route::post('addShipmentProduct','Admin\ShippingController@addShipmentProduct')->name('admin.addShipmentProduct');
	Route::get('shipment_product/{id}','Admin\ShippingController@getShipmentProduct')->name('admin.shipmentProduct');
	Route::post('editShipmentProduct','Admin\ShippingController@editShipmentProduct')->name('admin.editShipmentProduct');
	Route::post('changeShipmentProductStatus','Admin\ShippingController@changeShipmentProductStatus')->name('admin.changeShipmentProductStatus');
	Route::post('deleteShipmentProduct','Admin\ShippingController@deleteShipmentProduct')->name('admin.deleteShipmentProduct');

	Route::get('products', 'Admin\ProductController@getProducts')->name('admin.products');
	Route::post('addProduct','Admin\ProductController@addProduct')->name('admin.addProduct');

	Route::get('product/{product_id}','Admin\ProductController@getProduct')->name('admin.product');
	Route::post('editProduct','Admin\ProductController@editProduct')->name('admin.editProduct');

	Route::get('reports', 'Admin\ReportController@getReports')->name('admin.reports');
	Route::post('filterReports','Admin\ReportController@filterReports')->name('admin.filterReports');
	/*
		Routes for customers
	*/

	Route::get('customers','Admin\CustomerController@getCustomers')->name('admin.customers');

	Route::get('customer/{customer_id}','Admin\CustomerController@getCustomer')->name('admin.customer');

	Route::get('customer/{customer_id}/orders','Admin\CustomerController@getCustomerOrders' )->name('admin.customer.orderHistory');

	Route::get('customer/{customer_id}/transactions','Admin\CustomerController@getCustomerTransactions')->name('admin.customer.transactions');

	Route::post('searchCustomer','Admin\CustomerController@searchCustomer')->name('admin.searchCustomer');


	Route::post('filterOrders','Admin\OrderController@filterOrders')->name('admin.filterOrders');
	Route::get('filterOrders','Admin\OrderController@filterOrders')->name('admin.filterOrders');

	Route::post('filterTransactions','Admin\TransactionController@filterTransactions')->name('admin.filterTransactions');

	/*	
		Routes for Blog Posts
	*/
	Route::get('blog','Admin\BlogController@getBlogPosts')->name('admin.blog');
	Route::get('blog/{post_id}','Admin\BlogController@getBlogPost')->name('admin.blogPost');

	Route::view('addPost','admin.addPost');

	Route::post('addPost','Admin\BlogController@addPost')->name('admin.addPost');
	Route::post('editPost','Admin\BlogController@editPost')->name('admin.editPost');
	Route::post('deletePost','Admin\BlogController@deletePost')->name('admin.deletePost');



	/*
		Routes for FAQs
	*/
	Route::get('faqs','Admin\FAQController@getFAQs')->name('admin.faqs');
	Route::get('faq/{faq_id}','Admin\FAQController@getFAQ')->name('admin.faq');

	Route::view('addFAQ','admin.addFAQ')->name('admin.addFAQ');
	Route::post('addFAQ','Admin\FAQController@addFAQ')->name('admin.insertFAQ');
	Route::post('editFAQ','Admin\FAQController@editFAQ')->name('admin.editFAQ');
	Route::post('deleteFAQ','Admin\FAQController@deleteFAQ')->name('admin.deleteFAQ');

});
});

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/signUp', 'UserController@showRegisterForm')->name('signUp');

Route::post('registerUser','UserController@register')->name('userRegister');

//getting product list view
Route::get('products','ProductController@getProducts')->name('raha.products');
//getting particular product info view
Route::get('product/{product_id}','ProductController@getProduct')->name('item.product');
//getting FAQ view
Route::get('faq', 'FAQController@getFAQS')->name('raha.faq');

//getting cart view
Route::get('shopping/cart', function (){
    return view('cart');
})->name('raha.cart');

//getting blog view
Route::get('blog','BlogController@getBlogPosts')->name('raha.blog');

//getting blog article view
Route::get('blog/{id}','BlogController@getBlogPost')->name('raha.article');

//getting blog article view
Route::get('shopping/cart/checkout', function (){
    return view('checkout');
})->name('raha.checkout');


Route::group(['middleware'=>'auth'], function(){
	Route::get('confirm_order','OrderController@confirmOrder');
	Route::post('place_order','OrderController@placeOrder');
	// Route::post('complete_order','PaymentController@completeOrder');
	Route::post('complete_order','PaymentController@processWithPesaPal');
	Route::view('payment_options','payment')->name('payment_options');
		
});

Route::get('confirm_purchase',function(){
	return view('confirm_purchase');
})->name('raha.confirm_purchase');
Auth::routes();

Route::get('/home', function(){
    return redirect('/');
})->name('home');
// Route::get('logout','HomeController@logout');
Route::get('auth/logout', 'Auth\LoginController@logout')->name('auth.logout');

Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});

Route::post('register_user','Auth\RegisterController@register')->name('register_user');
// Route::get('donepayment', ['as' => 'paymentsuccess', 'uses'=>'PaymentsController@paymentsuccess']);
Route::get('payment_test','PaymentController@processWithPesaPal');
Route::get('pesapal_response', 'PaymentController@getPesapalResponse')->name('pesapal_response');
Route::get('pesapal_confirmation','PaymentController@transactionConfirmation')->name('pesapal_confirmation');

Route::get('auth/{driver}', ['as' => 'socialAuth', 'uses' => 'Auth\SocialController@redirectToProvider']);
Route::get('auth/{driver}/callback', ['as' => 'socialAuthCallback', 'uses' => 'Auth\SocialController@handleProviderCallback']);
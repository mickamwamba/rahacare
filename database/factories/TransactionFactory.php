<?php

use Faker\Generator as Faker;

$factory->define(App\Transaction::class, function (Faker $faker) {
   

    return [
        'amount' => 30000,
        'status' => 'SUCCESSFUL',
        'order_id' => 2,
        'transaction_method_id' => 1,
    ];
});
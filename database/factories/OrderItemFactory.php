<?php

use Faker\Generator as Faker;

$factory->define(App\OrderItem::class, function (Faker $faker) {
   

    return [
        'product_id' => 1,
        'order_id' => 2,
        'quantity' => 3,
        'size' => 30.21,
        'shipment_product_id'=>1,
    ];
});
<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateOrderPersonsAddressesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'order_persons_addresses';
    /**
     * Run the migrations.
     * @table order_persons_addresses
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('address')->nullable();
            $table->unsignedInteger('region_id');
            $table->unsignedInteger('order_person_id');
            $table->nullableTimestamps();
            
            $table->index(["order_person_id"], 'fk_order_persons_addresses_order_persons1_idx');

            $table->index(["region_id"], 'fk_order_persons_addresses_regions1_idx');


            $table->foreign('region_id', 'fk_order_persons_addresses_regions1_idx')
                ->references('id')->on('regions')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('order_person_id', 'fk_order_persons_addresses_order_persons1_idx')
                ->references('id')->on('order_persons')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}

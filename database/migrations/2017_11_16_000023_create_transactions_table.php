<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateTransactionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'transactions';
    /**
     * Run the migrations.
     * @table transactions
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->decimal('amount', 10, 2)->nullable();
            $table->enum('status', ['SUCCESSFUL', 'PENDING', 'FAILED'])->nullable();
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('transaction_method_id');
            $table->nullableTimestamps();
            
            $table->index(["order_id"], 'fk_transactions_orders1_idx');

            $table->index(["transaction_method_id"], 'fk_transactions_transaction_methods1_idx');


            $table->foreign('order_id', 'fk_transactions_orders1_idx')
                ->references('id')->on('orders')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('transaction_method_id', 'fk_transactions_transaction_methods1_idx')
                ->references('id')->on('transaction_methods')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}

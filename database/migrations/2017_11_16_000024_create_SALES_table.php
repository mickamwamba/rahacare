<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateSalesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'sales';
    /**
     * Run the migrations.
     * @table sales
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('status', ['COMPLETE', 'INCOMPLETE'])->nullable();
            $table->unsignedInteger('order_item_id');
            $table->nullableTimestamps();
            
            $table->index(["order_item_id"], 'fk_SALES_order_items1_idx');


            $table->foreign('order_item_id', 'fk_SALES_order_items1_idx')
                ->references('id')->on('order_items')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropProductIdFromOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('order_items', function (Blueprint $table) {
           $table->dropForeign('fk_order_items_products1_idx');
           $table->dropColumn('product_id');
           $table->integer('shipment_product_id')->unsigned();
           $table->foreign('shipment_product_id')->references('id')->on('shipment_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

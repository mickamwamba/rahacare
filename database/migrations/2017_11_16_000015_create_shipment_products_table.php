<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateShipmentProductsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'shipment_products';
    /**
     * Run the migrations.
     * @table shipment_products
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('quantity')->nullable();
            $table->string('unit_selling_price', 45)->nullable();
            $table->string('unit_buying_price', 45)->nullable();
            $table->unsignedInteger('shipment_id');
            $table->unsignedInteger('product_id');
            $table->nullableTimestamps();
            $table->index(["shipment_id"], 'fk_shipment_products_shipments1_idx');

            $table->index(["product_id"], 'fk_shipment_products_products1_idx');


            $table->foreign('shipment_id', 'fk_shipment_products_shipments1_idx')
                ->references('id')->on('shipments')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('product_id', 'fk_shipment_products_products1_idx')
                ->references('id')->on('products')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}

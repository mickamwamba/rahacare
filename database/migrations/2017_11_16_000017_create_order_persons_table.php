<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateOrderPersonsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'order_persons';
    /**
     * Run the migrations.
     * @table order_persons
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fname', 45)->nullable();
            $table->string('sname', 45)->nullable();
            $table->string('email')->nullable();
            $table->string('phone', 45)->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->nullableTimestamps();
            
            $table->index(["user_id"], 'fk_order_persons_users1_idx');


            $table->foreign('user_id', 'fk_order_persons_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}

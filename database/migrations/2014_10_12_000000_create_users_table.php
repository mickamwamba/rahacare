<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateOptionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'options';
    /**
     * Run the migrations.
     * @table options
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('value', 10)->nullable()->comment('eg. (Size : L, S, M)
(Color: Red, Green)');
            $table->unsignedInteger('option_group_id');
            $table->nullableTimestamps();
            $table->index(["option_group_id"], 'fk_options_option_groups1_idx');


            $table->foreign('option_group_id', 'fk_options_option_groups1_idx')
                ->references('id')->on('option_groups')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}

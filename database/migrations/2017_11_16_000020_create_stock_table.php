<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateStockTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'stock';
    /**
     * Run the migrations.
     * @table stock
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('available')->nullable()->comment('Remaining items in stock');
            $table->dateTime('last_update')->nullable();
            $table->unsignedInteger('shipment_product_id');
            $table->nullableTimestamps();
            
            $table->index(["shipment_product_id"], 'fk_stock_shipment_products1_idx');


            $table->foreign('shipment_product_id', 'fk_stock_shipment_products1_idx')
                ->references('id')->on('shipment_products')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}

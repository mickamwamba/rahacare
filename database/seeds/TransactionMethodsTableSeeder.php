<?php

use Illuminate\Database\Seeder;

class TransactionMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        factory(App\TransactionMethod::class, 3)->create();
    }
}

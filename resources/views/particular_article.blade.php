@extends('includes.master')

@section('content')
    @include('includes.white_bg_navbar')
    <div style="padding-top: 20px">
        <div class="center-cropped"
             style="background-image: url('{{$post->image}}'); background-size: cover">
        </div>
        <div class="container allMarginContainer">
            <h1 style="color: black; font-weight: bold">{{$post->title}}</h1>
            <div style="font-weight: bold; font-size: 25px; border-bottom: 1px solid rgba(211, 211, 211, 0.31)">
              
                Posted: {{date("D, d-M-Y H:i",strtotime($post->created_at))}}
            </div>
            <div><?php echo $post->content?></div>
        </div>
        {{--  <p class="container allMarginContainer" style="text-align:justify" >
            {{strip_tags($post->content)}}
        </p>  --}}
    </div>
@endsection
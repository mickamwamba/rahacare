@extends('includes.master')
@section('title','Blog')
@section('content')
    @include('includes.white_bg_navbar')
    <div class="container">
        <div class="row">
            {{-- one article view --}}
            @foreach($posts as $post)
            <article class="blog-article col-xl-4">
                <div>
                    <a href="/blog/{{$post->id}}" style="text-decoration: none; color: #696969">
                        <img class="thumbnail img-responsive" src="{{$post->image}}" width="100%" height="230px">
                        <div class="blog-info-txt" style="font-weight: bold">
                            <h4>{{$post->title}}</h4>
                            <p style="color: #C3DCE8">Posted: {{date("D, d-M-Y H:i",strtotime($post->created_at))}}</p>
                            <p id="article-paragraph">
                                {{strip_tags(substr($post->content,0,300))}}
                            </p>
                        </div>
                    </a>
                </div>
            </article>
            @endforeach
        </div>
    </div>
@endsection
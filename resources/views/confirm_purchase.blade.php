@extends('includes.master')

@section('content')
    @include('includes.checkout_navbar')
    <section class="container-fluid">
            @if (session('msg'))
                {{session('msg')}}
            @endif
        <div class="row">
            {{-- payment processes --}}
            <div class="col-md-8 payment-section-margin">
                <div class="tab">
                    <div class="container-fluid">
                        <div class="tab-group row">
                            <button class="tablinks col-md-4" onclick="openTab(event, 'customer')" id="defaultOpen">PURCHASE CONFIRMATION</button>
                            {{--  <button class="tablinks col-md-4" onclick="openTab(event, 'delivery')" id="defaultOpen">02 DELIVERY INFO</button>  --}}
                            {{--  <button class="tablinks col-md-4" onclick="openTab(event, 'payment')">02 PAYMENT SECTION  --}}
                            </button>
                        </div>
                    </div>

                 {{-- customer info tab--}}
                    <div id="customer" class="tabcontent">
                    
                      <h3 style="text-align:center">Congratulation, you have successfully paid for your order</h3>
                      <br>
                      <h5 style="text-align:center">You will soon receive a call from our agent and your parcel will be delivered with 7days of working days</h5>
                    <div style="text-align:center"><a href="{{route('home')}}"><button id="btnCheckOut"> <span>BACK TO HOME</span> </button></a></div>
                    </div>

                </div>
            </div>

            {{-- summery section --}}
            <div class="col-md-4 cart-summery" style="color: black;font-weight: bold">
                    <div class="allMarginContainer">
                        <p id="header-cart-summery">Summary</p>
                        <div class="row" id="fiftyPaddingTop">
                            <div class="col-md-6">
                                <p>Subtotal</p>
                            </div>
                            <div class="col-md-6">
                                <p id="subtotal">Tsh</p>
                            </div>
                        </div>
                        <div class="row" id="fiftyPaddingTop" style="border-bottom: 2px solid lightgray; padding-bottom: 20px">
                            <div class="col-md-6">
                                <p>Delivery</p>
                            </div>
                            <div class="col-md-6">
                                <p id="delivery_cost">Tsh</p>
                            </div>
                        </div>
                        <div class="row" id="fiftyPaddingTop">
                            <div class="col-md-6">
                                <p>Total</p>
                            </div>
                            <div class="col-md-6">
                                <p id="totalcost">Tsh</p>
                            </div>
                        </div>
                        {{--  <a id="btnCheckOut" href=""></a>  --}}
                        {{--  <form action="/complete_order" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_id" value="{{$order->id}}">
                            <input type="hidden" name="user_id" value="{{$user->id}}">  --}}
                            {{--  <button type="submit" id="btnCheckOut"> <span>CONTINUE TO PAYMENT</span> </button>  --}}

                        {{--  </form>  --}}
                    </div>
                </div>
        </div>
    </section>
    
@endsection

<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">

$(document).ready(function () {
    localStorage.removeItem("cart");
});



  </script>


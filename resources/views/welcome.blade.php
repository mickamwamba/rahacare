@extends('includes.master')
@section('title','Welcome')
@section('content')
        <section style="margin-right: 5px;">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        {{--  <a href="{{ url('/home') }}">Home</a>  --}}
                    @else
                        {{--  <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>  --}}
                    @endauth
                </div>
            @endif

            {{-- first section --}}
            <div class="" id="topDiv">
                {{-- header --}}
                @include('includes.transparent_header')
                <div class="title" id="topDivText">
                    <p>Live with confidence</p>
                    <a id="shopNow" href="{{ route('raha.products') }}"><span>SHOP NOW</span></a>
                </div>
            </div>

                {{-- second section--}}
            <section class="container-fluid" style="color: white; background-color: #F8FAFB">
                <div class="row">
                    {{-- welcome to raha --}}
                    <div class="col-md-6 allMarginContainer" id="txtWelcome">
                        <h3 class="text-center" id="fiftyPaddingTop">WELCOME TO RAHA</h3>
                        <p id="fiftyPaddingTop">We are RAHA! <br /> A team dedicated to help you find the right products for you after mastectomy treatment. We have built this site specifically to help you navigate through and find products that best fits your size and style. Feel free to navigate through and if you have any questions give us a call and we will be more than happy to help you.
                        </p>
                    </div>
                    {{-- image --}}
                    <div class="col-md-6" style="padding: 0">
                        <img class="img-responsive" src="images/web_assets/just_a_girl.jpg" width="100%" height="100%">
                    </div>
                </div>
            </section>

                {{-- third section --}}
            <div class="text-center allMarginContainer" style="background-color: #F8FAFB; color: black; font-weight: bold">
                <h2 id="fiftyPaddingTop">ABOUT US</h2>
                <p class="home-about-us" id="aboutUs">RAHA is a brand made with love by a team with a mission to bring quality post mastectomy products near to you. We currently have offices in Dar es Salaam Tanzania but we also ship our products to Kenya, Uganda, Rwanda, Burundi, Mozambique, Malawi and Zambia to name a few. 
                    We put a lot of efforts in the design process of our products with our customers at the center of our design thinking process. Our products are made to always give you confidence and help you approach every single day with full of enthusiasm.
                    
                </p>
                <div>
                    <p id="txt-read-more" style="padding-top: 50px" onclick="readMoreAbout()">READ MORE</p>
                    <i id="arrow-read-about" class="fa fa-angle-down"></i>
                </div>
            </div>

                {{--fourth section --}}
            <div class="text-center allMarginContainer" style="border-top: 1px solid lightgray; border-bottom: 1px solid lightgray">
                <h3>JOIN US</h3>
                <div class="row allMarginContainer">
                    <div class="col-md-4 social-media-icon">
                        <a href="https://web.facebook.com/RahaCare-155893591804506/?ref=br_rs">
                        <img src="images/blog/fb.png" width="60px" height="60px" onclick="window.open('https://www.fb.com/')">
                        </a>
                    </div>
                    <div class="col-md-4 social-media-icon">
                        <a href="https://twitter.com/RahaCare">
                        <img src="images/blog/twitter.png" width="65px" height="65px" onclick="window.open('https://www.twitter.com/')">
                    </a>
                    </div>
                    <div class="col-md-4 social-media-icon">
                        <a href="https://www.instagram.com/rahacare/">
                        <img src="images/blog/insta.png" width="60px" height="60px" onclick="window.open('https://www.instagram.com/')">
                    </a>
                    </div>
                </div>
            </div>

                {{-- fifth section --}}
            <div class="container-fluid" style="border-bottom: 1px solid lightgray; font-weight: bold">
                <a name="contact-us"></a>
                <div class="row">
                    <div class="col-sm-4 text-left text-not-center" id="borderRight">
                        <img class="footer-brand" src="/images/logo3.png" alt="">
                        <p>Our promise to you is responsive communication and transparency. When you call or e-mail us we respond right away.</p>
                        
                    </div>
                    <div class="col-sm-4 text-center" id="borderRight">
                        <p style="color: #F6CCCA;">CUSTOMER SERVICE</p>
                        <p>About US</p>
                        <p>Terms & Conditions</p>
                        <a href="{{ route('raha.faq') }}" style="color: #636b6f; text-decoration: none">FAQ</a>
                    </div>
                    <div class="col-sm-4 text-right text-not-center" style="padding: 30px">
                        <p style="color: #F6CCCA;">GET IN TOUCH</p>
                        <p>+255 756 606 060</p>
                        <p>info@rahacares.com</p>
                        <p>Dar es  Salaam, Tanzania</p>
                    </div>
                </div>
            </div>

            <footer class="text-center" style="padding: 10px; font-size: small"><p>Copy rights {{date("Y")}} &copy; Raha<strong>Reserved</strong></p></footer>
        </section>
@endsection
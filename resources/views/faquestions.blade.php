@extends('includes.master')

@section('content')
    @include('includes.white_bg_navbar')
    <div class="container faq-container">
        <p id="faq-welcome">Questions? Look here.</p>
        <p id="faq-welcome-info">Can’t find an answer? Call us at +255 (0) 756 606 060 or email info@rahacares.com</p>

        {{-- one qn container --}}
        <section id="accordion" role="tablist">
            {{-- one qn view --}}
            @foreach($faqs as $faq)
            <article id="faq-article" class="card">
                <div class="row" role="tab">
                    <p class="image-read-faq-{{$faq->id}}" id="image-read-faq" style="padding-top: 15px;">+</p>
                    <button onclick="changeSigns('image-read-faq-{{$faq->id}}')" type="button" data-toggle="collapse" data-target="#faq{{$faq->id}}" id="faq-header" style="background-color: white">
                        {{$faq->question}}
                    </button>
                </div>
                <div class="collapse faq-explanation" role="tabpanel" id="faq{{$faq->id}}" data-parent="#accordion">{{strip_tags($faq->answer)}}
                </div>
            </article>
            @endforeach
            {{--  <article id="faq-article" class="card">
                <div class="row" role="tab">
                    <p class="image-read-faq-2" id="image-read-faq" style="padding-top: 15px;">+</p>
                    <button onclick="changeSigns('image-read-faq-2', 4)" type="button" data-toggle="collapse" data-target="#faq2" id="faq-header" style="background-color: white">
                        Do i get a discount if i’m a frequent customer?
                    </button>
                </div>
                <div class="collapse faq-explanation" role="tabpanel" id="faq2" data-parent="#accordion">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                </div>
            </article>
            <article id="faq-article" class="card">
                <div class="row" role="tab">
                    <p class="image-read-faq-3" id="image-read-faq" style="padding-top: 15px;">+</p>
                    <button onclick="changeSigns('image-read-faq-3', 4)" type="button" data-toggle="collapse" data-target="#faq3" id="faq-header" style="background-color: white">
                        Can the item be delivered outside Dar es Salaam?
                    </button>
                </div>
                <div class="collapse faq-explanation" role="tabpanel" id="faq3" data-parent="#accordion">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                </div>
            </article>
            <article id="faq-article" class="card">
                <div class="row" role="tab">
                    <p class="image-read-faq-4" id="image-read-faq" style="padding-top: 15px;">+</p>
                    <button onclick="changeSigns('image-read-faq-4', 4)" type="button" data-toggle="collapse" data-target="#faq4" id="faq-header" style="background-color: white">
                        What are the breast forms made of?
                    </button>
                </div>
                <div class="collapse faq-explanation" role="tabpanel" id="faq4" data-parent="#accordion">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                </div>
            </article>  --}}
        </section>
    </div>
@endsection
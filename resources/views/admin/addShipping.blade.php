@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3 id='formheader'>Register new product shipping </h3>
      </div>

      <div>	
      	<div class="row">
      		<div class="col-sm-8 col-sm-offset-2 shipping-descriptions">
      			<p>Click the '+' sign below to add products to this shipment</p>
      		</div>
      	</div>

      		<!-- Modal -->
<div class="modal fade" id="addProductShipping" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add product to new shipping</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{route('admin.addShipmentProduct')}}" enctype="multipart/form-data">

            <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Product</label>
              	 <select id="" class="form-control" name="product_id" required>
              	   <option disabled>--Select category--</option>
			             @foreach($products as $product)
                      <option value="{{$product->id}}">{{$product->name}}</option>
                   @endforeach
	    	         </select>
             </div>

             <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Quanitity purchased</label>
              <input type="number" class="form-control form-custom" id="quantity" name="quantity" required>
             </div>

              <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Buying price per unit</label>
              <input type="number" class="form-control form-custom" id="unit_buying_price" name="unit_buying_price" required>
             </div>

              <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Selling price per unit</label>
              <input type="number" class="form-control form-custom" id="unit_selling_price" name="unit_selling_price" required>
             </div>


        {{ csrf_field() }}
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
      	<a href="#" data-toggle="modal" data-target="#addProductShipping"  class="btn btn-success btn-fab add-fab-btn">
			<i class="material-icons">add</i>
		</a>
	  </div>
    </div>

@endsection
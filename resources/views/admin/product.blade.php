@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Product - {{$product->name}} <span><img class="img-circle img-resize" src="{{$product->media?$product->media[0]->media:''}}"></span></h3>  
      </div>

      <div class="row">
      	<div class="summary-wrapper">
      		<div class="col-sm-6">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 			 <div class="panel-heading">Product summary</div>
						  <ul class="list-group">
						    <li class="list-group-item list-item-custom">Name: <span class="pull-right">{{$product->name}}</span></li>
						    <li class="list-group-item list-item-custom">Category: <span class="pull-right">{{$product->category->name}}</span></li>
						    <li class="list-group-item list-item-custom">Date Registered:<span class="pull-right">{{date("D,d-M-Y",strtotime($product->created_at))}}</span></li>
						    <li class="list-group-item list-item-custom">Available stock:<span class="pull-right">{{$product->stock?$product->stock->available:''}}</span></li>
						   
						  </ul>
					<div class="panel-foo"><a href="#" data-toggle="modal" data-target="#editProduct" class="btn btn-default btn-lg btn-block btn-raised btn-primary">Edit product</a></div>

				</div>
      		</div>

      		<div class="col-sm-6">
			    <div class="panel panel-default">
						  <!-- Default panel contents -->
						<div class="panel-heading">Product order summary</div>  	
		                 <table class="table table-striped table-hover animated fadeIn">
							  <thead>
									<tr>
									    <th>Day</th>
									    <th>Total orders</th>
									    <th>Order value</th>
									    
							 		 </tr>
							  </thead>
							  <tbody>
							  	<tr><th>Today</th> <td>{{$order_count['today']}}</td><td>{{$orders_summary['today']}}</td></tr>
							  	<tr><th>Yesterday</th><td>{{$order_count['yesterday']}}</td> <td>{{$orders_summary['yesterday']}}</td></tr>
							  	<tr><th>This week</th><td>{{$order_count['this_week']}}</td> <td>{{$orders_summary['this_week']}}</td></tr>
							  	<tr><th>Last week</th><td>{{$order_count['last_week']}}</td> <td>{{$orders_summary['last_week']}}</td></tr>
							  	<tr><th>This month</th> <td>{{$order_count['this_month']}}</td><td>{{$orders_summary['this_month']}}</td></tr>
							  	<tr><th>Last month</th> <td>{{$order_count['last_month']}}</td><td>{{$orders_summary['last_month']}}</td></tr>
													  		
							  </tbody>
						</table>
					</div>
      		</div>
      	</div>
      </div>

       <div class="row">
      	<div class="ordered-items">
      		<div class="col-sm-12">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 			<div class="panel-heading">Product Shipping History</div>
						@if(count($product->shipment_products)==0)
					      		<div class="content-not-found">
									<p>NO PRODUCT SHIPMENT HISTORY FOUND</p>
								</div>
						@else 

						<table class="table table-striped table-hover animated fadeIn">
					<thead>
						<tr>
							<th>#</th>
							<th>Shipping ID</th>
							<th>Date</th>
							<th>Quantity shipped</th>
					 	</tr>
					</thead>
					<tbody>
					  	@foreach($product->shipment_products as $shipment)
					  		<tr>
							    <td></td>
							    <td>{{$shipment->shipment_id}}</td>
							    <td>{{date("D, d-M-Y H:i",strtotime($shipment->created_at))}}</td>
							    <td>{{$shipment->quantity}}</td>
					  		</tr>
					  	@endforeach
					  		
					</tbody>
				</table>
				@endif
				</div>
      		</div>

      	</div>
      </div>


    </div>


  	<!-- Modal -->
<div class="modal fade" id="editProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Product</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{route('admin.editProduct')}}" enctype="multipart/form-data"> 
            
             <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Product name</label>
              <input type="text" class="form-control form-custom" id="name" name="name" value = "{{$product->name}}" required>
             </div>

             
             <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Product category</label>
              	 <select id="" class="form-control" name="category" required>
              	   <option disabled>--Select category--</option>
			             @foreach($categories as $category)
                      <option value="{{$category->id}}" {{($category->id==$product->category_id)?'selected="selected"':''}}>{{$category->name}}</option>
                   @endforeach
	    	     </select>
             </div>

             <div class="form-group">
			  <input type="file" id="inputFile4" multiple="" name="image">
			  <div class="input-group">
			    <input type="text" readonly="" class="form-control" placeholder="Select high quality product image">
			      <span class="input-group-btn input-group-sm">
			        <button type="button" class="btn btn-fab btn-fab-mini">
			          <i class="material-icons">attach_file</i>
			        </button>
			      </span>
			  </div>
			</div>

		<input type="hidden" name="product_id" value="{{$product->id}}">
        {{ csrf_field() }}
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

@endsection
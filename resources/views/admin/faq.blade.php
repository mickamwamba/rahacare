@extends('layouts.app')

@section('content')
	<div class="container">
      <div class="page-header">
        <h3>FAQ  <span class="pull-right"><a href="{{route('admin.faqs')}}"><button type="button" class="btn btn-lg btn-raised">BACK TO FAQs</button></a></span></h3>
      </div>

      <div class="row">
      	<div class="summary-wrapper post-wrapper col-md-10 col-md-offset-1">
      		<div> 
      			<label>Question:</label><h3>{{$faq->question}}</h3>
      		</div>
          <label>Answer</label>
      		<div class="blog-content">
      			<article>
      			 {{strip_tags($faq->answer)}}	
            </article>
                         
      		</div>

      		<div class="blog-actions">
      			<button type="button" class="btn btn-primary btn-lg btn-raised" data-toggle="modal" data-target="#editFAQ">EDIT FAQ</button>
				    <button type="button" class="btn btn-lg btn-raised btn-danger" data-toggle="modal" data-target="#deleteFAQ">DELETE FAQ</button>
      		</div>
      	</div>
      </div>
    </div>

    	<!-- Modal -->
<div class="modal fade" id="editFAQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit FAQ</h4>
      </div>
      <div class="modal-body">

         <form class="form-hor0izontal inner-form" method="POST" enctype="multipart/form-data" action="{{route('admin.editFAQ')}}">
          <fieldset>
            <div class='row'>
            <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="question" class="control-label">Question</label>
              <input type="text" class="form-control form-custom" id="question" name="question" value="{{$faq->question}}" required>
            </div>
         
            <div class="form-group" id="postpanel"><textarea  rows="15" cols="70" name="answer" id="content-add-ta" required>{{$faq->answer}} </textarea></div><br />

            <div class="row seperator">
                
            </div>
            <input type="hidden" name="faq_id" value="{{$faq->id}}">
                {{ csrf_field() }}
              <div class="form-group btn-wrapper">
                <div class="col-md-12">
                  <button type="submit" class="btn btn-raised btn-success btn-post" name="submit" value="submit">SAVE</button>
                </div>
              </div>
            </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteFAQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete this FAQ?</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
      	<form class="form-hor0izontal inner-form" method="POST" enctype="multipart/form-data" action="{{route('admin.deleteFAQ')}}">
      		<input type="hidden" name="faq_id" value="{{$faq->id}}">
      		 <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
      		 <button type="submit" class="btn btn-default">YES</button>
      	{{ csrf_field() }}
        </form>
      </div>
  </div>
</div>


@endsection

  <script type="text/javascript" src="{{URL::asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: "#content-add-ta",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ]
            });
  </script>
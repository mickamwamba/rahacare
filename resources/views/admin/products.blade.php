@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Products </h3>
      </div>

      <div class="row">
      		<div class="listing-wrapper">
            @if(count($products)==0)
              <div class="content-not-found">
                 <p>NO PRODUCT FOUND</p>
              </div>
            @else

      			<table class="table table-striped table-hover animated fadeIn">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Image</th>
              <th>Category</th>
							<th>Available stock</th>
							<th>Actions</th>
					 	</tr>
					</thead>
					<tbody>
					  	@foreach($products as $product)
					  		<tr>
							    <td></td>
							    <td>{{$product->name}}</td>
							    <td><img class="img-circle img-resize" src="{{count($product->media)>0?$product->media[0]->media:''}}" alt=""></td>
							    <td>{{$product->category->name}}</td>
                  <td>{{$product->stock?$product->stock->available:''}}</td>
							    <td><a href="{{route('admin.product',['id'=>$product->id])}}" class="btn btn-raised btn-primary btn-sm">VIEW PRODUCT</a></td>
					  		</tr>
					  	@endforeach
					  		
					</tbody>
				</table>
        <div class="list-pagination col-md-offset-1">{{$products->render()}}</div> 
        @endif
      		</div>
      		<a href="#" data-toggle="modal" data-target="#addProduct"  class="btn btn-success btn-fab add-fab-btn">
				<i class="material-icons">add</i>
			</a>
      </div>

  	</div>

  	<!-- Modal -->
<div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Register new product</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{route('admin.addProduct')}}" enctype="multipart/form-data"> 
            
             <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Product name</label>
              <input type="text" class="form-control form-custom" id="name" name="name" required>
             </div>

             <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Product category</label>
              	 <select id="" class="form-control" name="category" required>
              	   <option disabled>--Select category--</option>
			             @foreach($categories as $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                   @endforeach
	    	         </select>
             </div>

              <div class="form-group">
      			  <input type="file" id="inputFile4" multiple="" name="image" required>
      			  <div class="input-group">
      			    <input type="text" readonly="" class="form-control" placeholder="Select high quality product image">
      			      <span class="input-group-btn input-group-sm">
      			        <button type="button" class="btn btn-fab btn-fab-mini">
      			          <i class="material-icons">attach_file</i>
      			        </button>
      			      </span>
      			  </div>
      			</div>


        {{ csrf_field() }}
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>


@endsection
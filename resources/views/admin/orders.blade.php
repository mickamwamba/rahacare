@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Orders {{!empty($customer)?"- ".$customer->fname." ".$customer->sname:''}} </h3>
      </div>

      <div class="row">
      	<form method="POST" action="{{route('admin.filterOrders')}}">
      	<div class="filters">
      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <input placeholder="--From date--" name= "from_date" type="text" id="date-start" value="{{!empty($from)?$from:''}}" class="form-control datepicker end-date">
           		 </div> 
      		</div>

      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <input placeholder="--To date--" name= "to_date" type="text" id="date-end" value="{{!empty($to)?$to:''}}" class="form-control datepicker end-date">
           		 </div> 
      		</div>
      		<div class="col-sm-3">
      			<div class="form-group">
				    <label for="select111" class="col-md-2 control-label">Status</label>
				    <div class="col-md-10">
				        <select id="select111" class="form-control" name="status">
				        <option></option>
				        <option value="PROCESSED" {{!empty($status)&&$status=='PROCESSED'?'selected="selected"':''}}>COMPLETED</option>
				        <option value="PENDING" {{(!empty($status)&&$status=='PENDING')?'selected="selected"':''}}>PENDING</option>
				        <option value="CANCELED" {{!empty($status)&&$status=='CANCELED'?'selected="selected"':''}}>CANCELED</option>
				        </select>
				    </div>
				</div>
      		</div>
      		@if(!empty($customer))
      			<input type="hidden" name="customer_id" value="{{$customer->id}}">
      		@endif
      		  {{ csrf_field() }}
      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <button class="btn btn-raised btn-default btn-xs" type="submit"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span>FILTER</button>
           		 </div> 
      		</div>

      	</div>	
    	</form>
      </div>
      @if(!empty($from))
 			{{--  <div class="search-result-text"><h4>{{$status}} orders from {{$from}} to {{$to}}</h4></div>     		  --}}
      @endif

      <div class="row">
      		<div class="listing-wrapper">
      		  @if(count($orders)==0)
		      		<div class="content-not-found">
						<p>NO ORDER HISTORY FOUND</p>
					</div>
			  @else 
      			<table class="table table-striped table-hover animated fadeIn">
					<thead>
						<tr>
							<th>#</th>
							<th>Order number</th>
							<th>Order Person</th>
							<th>Order value</th>
							<th>Time</th>
							<th>Status</th>
							<th>Payment status</th>
							<th>Actions</th>
					 	</tr>
					</thead>
					<tbody>
					  	@foreach($orders as $order)
					  		<tr>
							    <td></td>
							    <td>{{$order->id}}</td>
							    <td>{{!empty($customer)?$customer->fname." ".$customer->sname:$order->customer->fname." ".$order->customer->sname}}</td>
							    <td>{{$order->value}}</td>
							    <td>{{date("D, d-M-Y H:i",strtotime($order->created_at))}}</td>
								<td>{{$order->status}}</td>
								<td> <span class="{{$order->paid?'paid-order':'unpaid-order'}}"> {{$order->paid?'PAID':'NOT PAID'}} </span> </td>								
							    <td><a href="{{route('admin.order',['id'=>$order->id])}}" class="btn btn-raised btn-primary btn-sm">VIEW ORDER</a></td>
					  		</tr>
					  	@endforeach
					  		
					</tbody>
				</table>
				 <div class="list-pagination col-md-offset-1">{{empty($customer)?$orders->render():''}}</div> 
				@endif
      		</div>
      </div>

    </div>  
@endsection
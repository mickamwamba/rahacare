@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Profile - {{$customer->fname}} {{$customer->sname}} </h3>
      </div>

      <div class="row">
      	<div class="summary-wrapper">
      		<div class="col-sm-12">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 			 <div class="panel-heading">PERSONAL DETAILS</div>
						  <ul class="list-group">
						    <li class="list-group-item list-item-custom">Name: <span class="pull-right">{{$customer->fname}} {{$customer->sname}}</span></li>
						    <li class="list-group-item list-item-custom">Email: <span class="pull-right">{{$customer->email}}</span></li>
						    <li class="list-group-item list-item-custom">Phone:<span class="pull-right">{{$customer->phone}}</span></li>
						    <li class="list-group-item list-item-custom">Location:<span class="pull-right"></span></li>   
						  </ul>
				</div>
      		</div>

      	</div>
      </div>

      <div class="row">
      	<div class="summary-wrapper">
      		
      		<div class="col-sm-12">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 		<div class="panel-heading">CUSTOMER ORDER HISTORY</div>
			 		@if(count($customer->orders)==0)
				      	<div class="content-not-found">
							<p>NO CUSTOMER ORDER FOUND</p>
						</div>
					@else

			 		<table class="table table-striped table-hover animated fadeIn">
					<thead>
						<tr>
							<th>Order#</th>
							<th>Order value</th>
							<th>Order date</th>
							<th>Status</th>
							<th>Actions</th>
					 	</tr>
					</thead>
					<tbody>
					  	@foreach($customer->orders as $order)
					  		<tr>
							    <td>{{$order->id}}</td>
							    <td>{{$order->value}}</td>
							    <td>{{date("D, d-M-Y H:i",strtotime($order->created_at))}}</td>
							    <td>{{$order->status}}</td>
							    <td><a href="{{route('admin.order',['id'=>$order->id])}}" class="btn btn-raised btn-primary btn-sm">GO TO ORDER</a></td>
					  		</tr>
					  	@endforeach
					  		
					</tbody>
				</table>
				@endif
				<div class="panel-foo"><a href="{{route('admin.customer.orderHistory',['id'=>$customer->id])}}" class="btn btn-default btn-lg btn-block btn-raised btn-primary">VIEW FULL ORDER HISTORY</a></div>
				</div>
      		</div>
      	</div>
      </div>

      <div class="row">
      	<div class="summary-wrapper">
      		
      		<div class="col-sm-12">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 		<div class="panel-heading">CUSTOMER TRANSACTIONS HISTORY</div>
			 		@if(count($customer->transactions)==0)
				      	<div class="content-not-found">
							<p>NO CUSTOMER TRANSACTION FOUND</p>
						</div>
					@else
			 		<table class="table table-striped table-hover animated fadeIn">
					<thead>
						<tr>
							<th>Transaction ID</th>
							<th>Amount</th>
							<th>Method</th>
							<th>Date</th>
							<th>Status</th>
					 	</tr>
					</thead>
					<tbody>
					  
					  	@foreach($customer->transactions as $transaction)
					  		<tr>
							    <td>{{$transaction->id}}</td>
							    <td>{{$transaction->amount}}</td>
							    <td>{{$transaction->method?$transaction->method->name:''}}</td>
							    <td> {{date("D, d-M-Y H:i")}}</td>
								<td>{{$transaction->status}}</td>							    
					  		</tr>
					  	@endforeach
					</tbody>
				</table>
				@endif
				<div class="panel-foo"><a href="{{route('admin.customer.transactions',['id'=>$customer->id])}}" class="btn btn-default btn-lg btn-block btn-raised btn-primary">VIEW FULL TRANSACTIONS HISTORY</a></div>
				</div>
      		</div>
      	</div>
      </div>


    </div>
@endsection
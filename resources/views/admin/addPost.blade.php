@extends('layouts.app')

@section('content')
	<div class="container">
      <div class="page-header">
        <h3>New Blog Post </h3>
      </div>
    
      <div class="row"> 
      	<div class="summary-wrapper col-md-10 col-md-offset-1">
      		 <form class="form-hor0izontal inner-form" method="POST" enctype="multipart/form-data" action="{{route('admin.addPost')}}">
          <fieldset>
            <div class='row'>
            <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="inputEmail" class="control-label">Title</label>
              <input type="text" class="form-control form-custom" id="Title" name="title" value="" required>
            </div>
         
            <div class="form-group" id="postpanel"><textarea  rows="15" cols="70" name="content" id="content-add-ta" required> </textarea></div><br />


            <div class="form-group">
      			  <input type="file" name="image" id="inputFile4" multiple="" required>
      			  <div class="input-group">
      			    <input type="text" readonly="" class="form-control" placeholder="Upload image">
      			      <span class="input-group-btn input-group-sm">
      			        <button type="button" class="btn btn-fab btn-fab-mini">
      			          <i class="material-icons">attach_file</i>
      			        </button>
      			      </span>
      			  </div>
      			</div>

               <div class="row seperator">
                
              </div>
                {{ csrf_field() }}
              <div class="form-group btn-wrapper">
                <div class="col-md-12">
                  <button type="submit" class="btn btn-raised btn-success btn-post" name="submit" value="submit">post</button>
                </div>
              </div>
            </div>
        </form>
      	</div>
      	
      </div>
    </div>

@endsection

 {{--  <script type="text/javascript" src="{{URL::asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>  --}}
 <script type="text/javascript" src="{{URL::asset('tinymce/tinymce.min.js')}}"></script>

 <script type="text/javascript">
            tinymce.init({
                selector: "#content-add-ta",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ]
            });
 </script>
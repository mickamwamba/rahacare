@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Blog </h3>
      </div>

    <div class="row">
      	<div class="listing-wrapper">

      		<div class="list-group">

			  @if(count($posts)==0)
		      		<div class="content-not-found">
						<p>NO BLOG POST FOUND</p>
					</div>
			  @else 
	      	  @foreach($posts as $post)
				  <div class="list-group-item">
				    <div class="row-picture">
				      <img class="circle" src="{{$post->image}}" alt="icon">
				    </div>
				    <div class="row-content">
				      <h4 class="list-group-item-heading"><a href="{{route('admin.blogPost',['id'=>$post->id])}}">{{$post->title}}</a></h4>

				      <p class="list-group-item-text" style="text-align: justify;">{{strip_tags(substr($post->content,0,300))}}</p>
				      <span class="blog-date">Posted: {{date("D, d-M-Y H:i",strtotime($post->created_at))}}</span>
				    </div>
				  </div>
				  <div class="list-group-separator"></div>
			  @endforeach
			  <div class="list-pagination col-md-offset-1">{{$posts->render()}}</div> 
			  @endif

		</div>
		<a href="{{route('admin.addPost')}}" class="btn btn-success btn-fab add-fab-btn">
			<i class="material-icons">add</i>
		</a>
    </div>
    </div>
@endsection
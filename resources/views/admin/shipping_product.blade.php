@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Product shipment details </h3>
      </div>
    </div>
    <div class="row">

      		<div class="col-sm-8 col-sm-offset-2">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 			 <div class="panel-heading">Product</div>
						  <ul class="list-group">
						    <li class="list-group-item list-item-custom">Name: <span class="pull-right">{{$product->product->name}}</span></li>
						    <li class="list-group-item list-item-custom">Quantity: <span class="pull-right">{{$product->quantity}}</span></li>
						    <li class="list-group-item list-item-custom">Unit buying price:<span class="pull-right">{{$product->unit_buying_price}}</span></li>
						    <li class="list-group-item list-item-custom">Unit selling price:<span class="pull-right">{{$product->unit_selling_price}}</span></li>
						    <li class="list-group-item list-item-custom">Status: <span class="pull-right">{{$product->status}}</span></li>
						  </ul>
					<div class="panel-foo">
						<div class="btn-group btn-group-justified btn-group-raised">
						  <a href="#" data-toggle="modal" data-target="#editShipmentProduct" class="btn btn-primary">EDIT</a>
						  <a href="#" data-toggle="modal" data-target="#changeShipmentProductStatus" class="btn ">{{$product->status=='ACTIVE'?'DISABLE':'ACTIVATE'}}</a>
						  <a href="#" data-toggle="modal" data-target="#deleteShipmentProduct" class="btn btn-danger">DELETE</a>
						</div>

					</div>

				</div>
      		</div>

      </div>

            		<!-- Modal -->
<div class="modal fade" id="editShipmentProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add product to new shipping</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{route('admin.editShipmentProduct')}}" enctype="multipart/form-data">

            <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Product</label>
              <input type="text" class="form-control form-custom" id="product" name="product_name" value="{{$product->product->name}}" required disabled>
             </div>

             <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Quanitity purchased</label>
              <input type="number" class="form-control form-custom" id="quantity" name="quantity" value= "{{$product->quantity}}" required>
             </div>

              <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Buying price per unit</label>
              <input type="number" class="form-control form-custom" id="unit_buying_price" name="unit_buying_price" value="{{$product->unit_buying_price}}" required>
             </div>

              <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Selling price per unit</label>
              <input type="number" class="form-control form-custom" id="unit_selling_price" name="unit_selling_price" value="{{$product->unit_selling_price}}" required>
             </div>

        <input type="hidden" name="shipment_product_id" value="{{$product->id}}">
        {{ csrf_field() }}
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

            		<!-- Modal -->
<div class="modal fade" id="changeShipmentProductStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{{$product->status=='ACTIVE'?'DISABLE':'ACTIVATE'}} THIS PRODUCT ?</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{route('admin.changeShipmentProductStatus')}}" enctype="multipart/form-data">
       	<input type="hidden" name="status" value="{{$product->status=='ACTIVE'?'DISABLED':'ACTIVE'}}">
        <input type="hidden" name="shipment_product_id" value="{{$product->id}}">
        {{ csrf_field() }}
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

            		<!-- Modal -->
<div class="modal fade" id="deleteShipmentProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">DELETE THIS PRODUCT ?</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{route('admin.deleteShipmentProduct')}}" enctype="multipart/form-data">
        <input type="hidden" name="shipment_product_id" value="{{$product->id}}">
        {{ csrf_field() }}
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Delete</button>
      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

@endsection
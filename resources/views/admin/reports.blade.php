@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Reports </h3> <span class="pull-right"><a href="javascript:void(0)" class="btn btn-raised btn-default">Export pdf</a></span>
      </div>
      <div>
      <div class="row">
					<form method="POST" action="{{route('admin.filterReports')}}">

      	<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <input placeholder="--From date--" name="from" value="{{!empty($from)&&$from!=''?$from:''}}" type="text" id="date-start"  class="form-control datepicker end-date">
           		 </div> 
      		</div>

      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <input placeholder="--To date--" name="to" value="{{!empty($to)&&$to!=''?$to:''}}" type="text" id="date-end"  class="form-control datepicker end-date">
           		 </div> 
					</div>
					{{ csrf_field() }}
      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <button class="btn btn-raised btn-default btn-xs" type="submit"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span>FILTER</button>
           		 </div> 
					</div>
				</form>
      </div>
      	
      </div>
      <div class="row">
      	<div class="summary-wrapper">
      		<div class="col-sm-6">
      			<div class="reports-wrapper">
      				<div class="panel panel-default">
						  <!-- Default panel contents -->
						<div class="panel-heading">ORDERS<span></span></div>  	
		                 <ul class="list-group">
						    <li class="list-group-item list-item-custom">Total order: <span class="pull-right">{{count($reports['orders'])}}</span></li>
						    <li class="list-group-item list-item-custom">Order value: <span class="pull-right">{{$orderCost}}</span></li>
						    <li class="list-group-item list-item-custom">Successful:<span class="pull-right">{{$reports['orders']->where('status','PROCESSED')->count()}}</span></li>
						    <li class="list-group-item list-item-custom">Pending:<span class="pull-right">{{$reports['orders']->where('status','PENDING')->count()}}</span></li>
						   
						  </ul>
						<div class="panel-foo"></div>
					</div>
      			</div>
      		</div>

      		<div class="col-sm-6">
      			<div class="reports-wrapper">
      				<div class="panel panel-default">
						  <!-- Default panel contents -->
						<div class="panel-heading">TRANSACTIONS<span></span></div>  	
		                 <ul class="list-group">
						    <li class="list-group-item list-item-custom">Total transactions: <span class="pull-right">{{count($reports['transactions'])}}</span></li>
						    <li class="list-group-item list-item-custom">Transactions value: <span class="pull-right">{{$reports['transactions']->sum('amount')}}</span></li>
						    <li class="list-group-item list-item-custom">Successful:<span class="pull-right">{{$reports['transactions']->where('status','SUCCESSFUL')->count()}}</span></li>
						    <li class="list-group-item list-item-custom">Pending:<span class="pull-right">{{$reports['transactions']->where('status','PENDING')->count()}}</span></li>
						   
						  </ul>
						<div class="panel-foo"></div>
					</div>
      			</div>
      		</div>

      	</div>
      </div>

      <div class="row">
      	<div class="summary-wrapper">
      		<div class="col-sm-6">
      			<div class="reports-wrapper">
      				<div class="panel panel-default">
						  <!-- Default panel contents -->
						<div class="panel-heading">SALES<span></span></div>  	
		                 <ul class="list-group">
						    <li class="list-group-item list-item-custom">Products sold: <span class="pull-right">{{$total_sales['products']}}</span></li>
						    <li class="list-group-item list-item-custom">Revenue: <span class="pull-right">{{$total_sales['revenue']}}</span></li>
						    <li class="list-group-item list-item-custom">:<span class="pull-right"></span></li>
						    <li class="list-group-item list-item-custom">:<span class="pull-right"></span></li>
						   
						  </ul>
						<div class="panel-foo"></div>
					</div>
      			</div>
      		</div>

      		<div class="col-sm-6">
      			<div class="reports-wrapper">
      				<div class="panel panel-default">
						  <!-- Default panel contents -->
						<div class="panel-heading">CUSTOMERS<span></span></div>  	
		                 <ul class="list-group">
						    <li class="list-group-item list-item-custom">Total customers: <span class="pull-right">{{$reports['customers']->count()}}</span></li>
						    <li class="list-group-item list-item-custom">:<span class="pull-right"></span></li>
						    <li class="list-group-item list-item-custom">:<span class="pull-right"></span></li>
						    <li class="list-group-item list-item-custom">:<span class="pull-right"></span></li>
						   
						  </ul>
						<div class="panel-foo"></div> 
					</div>
      			</div>
      		</div>

      	</div>
      </div>


    </div>
@endsection
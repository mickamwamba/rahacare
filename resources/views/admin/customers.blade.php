@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Customers </h3>
      </div>

      <div class="row">
      	<div class="filters">
      		<div class="col-lg-6">
      			<form action="{{route('admin.searchCustomer')}}" method="POST">
			    <div class="input-group label-floating">
			      <input type="text" name="user_input" value="{{(!empty($user_input))?$user_input:''}}" class="form-control" placeholder="Search customer #name, #email, #phone..." aria-label="Search for..." required>
			      <span class="input-group-btn">
			        <button class="btn btn-secondary btn-raised" type="submit">SEARCH</button>
			      </span>
			           {{ csrf_field() }}
			    </div>
			    </form>
			</div>

      	</div>	
   
      </div>

      <div class="row">
      	@if(!empty($user_input))
 			<div class="search-result-text"><h4>Search results for '{{$user_input}}'</h4></div>     		
      	@endif
      	
      		<div class="listing-wrapper">
      			@if(count($customers)==0)
		      		<div class="content-not-found">
						<p>NO CUSTOMER FOUND</p>
					</div>
				@else
      			<table class="table table-striped table-hover animated fadeIn">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Date registered</th>
			
							<th>Actions</th>
					 	</tr>
					</thead>
					<tbody>
					  	@foreach($customers as $customer)
					  		<tr>
							    <td></td>
							    <td>{{$customer->fname}} {{$customer->sname}}</td>
							    <td>{{$customer->email}}</td>
							    <td>{{$customer->phone}}</td>
							    <td> {{date("D,d-M-Y",strtotime($customer->created_at))}}</td>
							    <td><a href="{{route('admin.customer',['id'=>$customer->id])}}" class="btn btn-raised btn-primary btn-sm">VIEW PROFILE</a></td>
					  		</tr>
					  	@endforeach
					  		
					</tbody>
				</table>
				 <div class="list-pagination col-md-offset-1">{{$customers->render()}}</div> 
				@endif
      		</div>
      </div>

    </div>  
@endsection
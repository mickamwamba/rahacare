@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Order - #{{$order->id}} </h3>
      </div>

      <div class="row">
      	<div class="summary-wrapper">
      		<div class="col-sm-6">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 			 <div class="panel-heading">Order summary</div>
						  <ul class="list-group">
						    <li class="list-group-item list-item-custom">Order #: <span class="pull-right">{{$order->id}}</span></li>
						    <li class="list-group-item list-item-custom">Order date: <span class="pull-right">{{date("D, d-M-Y H:i")}}</span></li>
						    <li class="list-group-item list-item-custom">Payment status:<span class="pull-right {{($order->paid)?'paid-order':'unpaid-order'}}">{{$order->paid?'PAID':'NOT PAID'}}</span></li>
						    <li class="list-group-item list-item-custom">Status:<span class="pull-right">{{$order->status}}</span></li>
						   
						  </ul>
				</div>
      		</div>

      		<div class="col-sm-6">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 			 <div class="panel-heading">Order person</div>
						  <ul class="list-group">
						    <li class="list-group-item list-item-custom">Name: <span class="pull-right">{{$order->customer->fname}} {{$order->customer->sname}}</span></li>
						    <li class="list-group-item list-item-custom">Phone#: <span class="pull-right">{{$order->customer->phone}}</span></li>
						    <li class="list-group-item list-item-custom">Email:<span class="pull-right">{{$order->customer->email}}</span></li>
						    <li class="list-group-item list-item-custom">Location:<span class="pull-right">{{($order->customer->region)?$order->customer->region->name:''}}, Tanzania</span></li>
						   
						  </ul>
				</div>
      		</div>
      	</div>
      </div>

      <div class="row">
      	<div class="ordered-items">
      		<div class="col-sm-12">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 			<div class="panel-heading">Ordered items</div>
						@if(count($order->items)==0)
		      				<div class="content-not-found">
									<p>NO ORDER ITEMS FOUND</p>
							</div>
						  @else 
						<table class="table table-striped table-hover animated fadeIn">
					<thead>
						<tr>
							<th>#</th>
							<th>Product</th>
							<th>Size</th>
							<th>Color</th>
							<th>Quantity</th>

							<th>Stock available</th>
							
							<th>Status</th>
							<th><div class="form-group select-all" style="display:{{$order->status=='PROCESSED'?'none':'block'}}">
						      <div class="checkbox checkbox-wrapper">
						      <label>
						      <input type="checkbox" id="select_all" onclick="selectAll()">
						      </label>
						      <span>Dispatch all</span>
						      </div>
						      </div>
						  </th>
					 	</tr>
					</thead>
					<tbody>
							@foreach($order->items as $item)
								
					  		<tr>
							    <td></td>
							    <td>{{$item->shipment_product->product->name}}</td>
									 <td>{{$item->size}}</td>
							     <td>{{$item->color}}</td>									 
							    <td>{{$item->quantity}}</td>
							    <td>{{$item->shipment_product->product->stock->available}}</td>
							
							    <td>{{$item->status}}</td>
							 	<td><div class="form-group" style="display:{{$item->status=='DISPATCHED'?'none':'block'}}">
							    <div class="checkbox">
							    <label>
							    <input type="checkbox" name="action" value="{{$item->id}}">
							    </label>
							    </div>
							    </div>
							    </td>
					  		</tr>
					  	@endforeach
					  		
					</tbody>
				</table>
				<a href="javascript:void(0)" class="btn btn-raised btn-primary pull-right"  onclick="dispatchItems()" style="display:{{$order->status=='PROCESSED'?'none':'block'}}">Dispatch all selected</a>
				@endif
				
				</div>
      		</div>

      	</div>
      </div>
      

    </div>
@endsection
<script type="text/javascript">
	
	function selectAll(source){
      var checkbox;
      var select_all = document.getElementById('select_all');

      checkboxes = document.getElementsByName('action');
        for (var i = 0; i < checkboxes.length; i++) {
          if (select_all.checked==true) {
            checkboxes[i].checked = true;
          }
          else{
            checkboxes[i].checked = false;
          }
      }
    }

    function dispatchItems(){
			var paid = <?php echo $order->paid;?>;
			if(!paid)
			{
				Snackbar.show({text: 'Order is not paid, you can not dispatch it',
				//actionText: 'PROCEED',
				//onActionClick: function(){
				//		 window.location = "{{ route('raha.cart') }}";
				//},
				
				}); 
			return;
				
			}

      var method = "POST";
      var path = "/dispatchItems";
      var selected = new Array();
      checkboxes = document.getElementsByName('action');
      for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
          // selected.push(checkboxes[i].value);
            selected[i] = checkboxes[i].value;
        }
      }
      var hiddenField;
      var form = document.createElement("form");
       form.setAttribute("method", method);
       form.setAttribute("action", path);

       for (var i = 0; i < selected.length; i++) {
         hiddenField = document.createElement("input");
         hiddenField.setAttribute("type", "hidden");
         hiddenField.setAttribute("name", 'items[]');  
         hiddenField.setAttribute("value", selected[i]);
         form.appendChild(hiddenField);

         orderField = document.createElement("input");
         orderField.setAttribute("type", "hidden");
         orderField.setAttribute("name", 'order_id');  
         orderField.setAttribute("value", "<?=$order->id?>");
         form.appendChild(orderField);
			 }
			 tokenField = document.createElement("input");
			 tokenField.setAttribute("type", "hidden");
			 tokenField.setAttribute("name", '_token');  
			 tokenField.setAttribute("value", "<?=csrf_token()?>");
			 form.appendChild(tokenField);
       document.body.appendChild(form);
       form.submit();
    }


</script>
@extends('layouts.app')

@section('content')
	<div class="container">
      <div class="page-header">
        <h3>Blog Post </h3>
      </div>

      <div class="row">
      	<div class="summary-wrapper post-wrapper col-md-10 col-md-offset-1">
      		<div> 
      			<h3>{{$post->title}}</h3> 
      			<span class="blog-date">Posted on: {{date("D, d-M-Y H:i",strtotime($post->created_at))}}</span>
      		</div>

      		<div class="post-image" style="background-image: url({{$post->image}});">
      		</div>

      		<div class="blog-content">
      			<article>
                 {{--  {{($post->content) }}  --}}
                 <?php echo $post->content?>
            </article>
                         
      		</div>

      		<div class="blog-actions">
      			<button type="button" class="btn btn-primary btn-lg btn-raised" data-toggle="modal" data-target="#editPost">EDIT POST</button>
				    <button type="button" class="btn btn-lg btn-raised btn-danger" data-toggle="modal" data-target="#deletePost">DELETE POST</button>
      		</div>
      	</div>
      </div>
    </div>

    	<!-- Modal -->
<div class="modal fade" id="editPost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Blog Post</h4>
      </div>
      <div class="modal-body">

         <form class="form-hor0izontal inner-form" method="POST" enctype="multipart/form-data" action="{{route('admin.editPost')}}">
          <fieldset>
            <div class='row'>
            <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="inputEmail" class="control-label">Title</label>
              <input type="text" class="form-control form-custom" id="Title" name="title" value="{{$post->title}}" required>
            </div>
         
              <div class="form-group" id="postpanel"><textarea  rows="15" cols="70" name="content" id="content-add-ta" required> {{$post->content}} </textarea></div><br />


            <div class="form-group">
      			  <input type="file" id="inputFile4" name ="image" multiple="">
      			  <div class="input-group">
      			    <input type="text" readonly="" class="form-control" placeholder="Upload image">
      			      <span class="input-group-btn input-group-sm">
      			        <button type="button" class="btn btn-fab btn-fab-mini">
      			          <i class="material-icons">attach_file</i>
      			        </button>
      			      </span>
      			  </div>
      			</div>

               <div class="row seperator">
                
              </div>
              <input type="hidden" name="post_id" value="{{$post->id}}">
                {{ csrf_field() }}
              <div class="form-group btn-wrapper">
                <div class="col-md-12">
                  <button type="submit" class="btn btn-raised btn-success btn-post" name="submit" value="submit">post</button>
                </div>
              </div>
            </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deletePost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete this post?</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
      	<form class="form-hor0izontal inner-form" method="POST" enctype="multipart/form-data" action="{{route('admin.deletePost')}}">
      		<input type="hidden" name="post_id" value="{{$post->id}}">
            {{ csrf_field() }}      		
           <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
      		 <button type="submit" class="btn btn-default">YES</button>
      	</form>
      </div>
  </div>
</div>


@endsection

  {{--  <script type="text/javascript" src="{{URL::asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>  --}}
  <script type="text/javascript" src="{{URL::asset('tinymce/tinymce.min.js')}}"></script>

  <script type="text/javascript">
            tinymce.init({
                selector: "#content-add-ta",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ]
            });
  </script>
@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3 id='formheader'>Shipping - #{{$shipment->id}} </h3>
      </div>

      <div>	
      	<div class="row">
          <div class="listing-wrapper col-md-12">

            <div class="panel panel-default">
              <!-- Default panel contents -->
            <div class="panel-heading">Product</div>    
                     <table class="table table-striped table-hover animated fadeIn">
                <thead>
                  <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Quantity</th>
                      <th>Unit buying price</th>
                      <th>Unit selling price</th>
                      <th>Status</th>
                      <th></th>
                      
                   </tr>
                </thead>
                <tbody>
                  @foreach($shipment->products as $product)
                    <tr>
                      <td></td>
                      <td>{{$product->product->name}}</td>
                      <td>{{$product->quantity}}</td>
                      <td>{{$product->unit_buying_price}}</td>
                      <td>{{$product->unit_selling_price}}</td>
                       <td>{{$product->status}}</td>
                       <td>
                        <div class="btn-group">
                            <a href="{{route('admin.shipmentProduct',['id'=>$product->id])}}" class="btn btn-primary btn-raised">VIEW DETAILS</a>
                          </div>
                      </td>
                    </tr>

                  @endforeach
                </tbody>
            </table>
            <!-- <div class="panel-foo"><a href="/admin/orders" class="btn btn-default btn-lg btn-block btn-raised btn-primary">View all orders</a></div> -->
          </div>


          </div>
      	</div>

      		<!-- Modal -->
<div class="modal fade" id="addProductShipping" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add product to new shipping</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{route('admin.addShipmentProduct')}}" enctype="multipart/form-data">

            <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Product</label>
              	  <select id="" class="form-control" name="product_id" required>
                   <option disabled>--Select category--</option>
                   @foreach($products as $product)
                      <option value="{{$product->id}}">{{$product->name}}</option>
                   @endforeach
                 </select>
             </div>

             <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Quanitity purchased</label>
              <input type="number" class="form-control form-custom" id="quantity" name="quantity" required>
             </div>

              <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Buying price per unit</label>
              <input type="number" class="form-control form-custom" id="unit_buying_price" name="unit_buying_price" required>
             </div>

              <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="product" class="control-label">Selling price per unit</label>
              <input type="number" class="form-control form-custom" id="unit_selling_price" name="unit_selling_price" required>
             </div>

        <input type="hidden" name="shipping_id" value="{{$shipment->id}}">
        {{ csrf_field() }}
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
      	<a href="#" data-toggle="modal" data-target="#addProductShipping"  class="btn btn-success btn-fab add-fab-btn">
			<i class="material-icons">add</i>
		</a>
	  </div>
    </div>

@endsection
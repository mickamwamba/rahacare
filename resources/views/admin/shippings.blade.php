@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Shipping history </h3>
      </div>

      {{--  <div class="row">
      	<div class="filters">
      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <input placeholder="--From date--" name= "time" type="text" id="date-start"  class="form-control datepicker end-date">
           		 </div> 
      		</div>

      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <input placeholder="--To date--" name= "time" type="text" id="date-end"  class="form-control datepicker end-date">
           		 </div> 
      		</div>

      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <a href="javascript:void(0)" class="btn btn-raised btn-default btn-xs"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span>FILTER</a>
           		 </div> 
      		</div>

      	</div>	
   
      </div>  --}}

      <div class="row">
      		<div class="listing-wrapper">
      			<table class="table table-striped table-hover animated fadeIn">
					<thead>
						<tr>
							<th>#</th>
							<th>Shipping ID</th>
							<th>Date</th>
							<th>Products shipped</th>
							<th>Cost</th>
							<th>Actions</th>
					 	</tr>
					</thead>
					<tbody>
						@foreach( $shipments as $shipment)
					  		<tr>
							    <td></td>
							    <td>{{$shipment->id}}</td>
							    <td>{{date("d-M-Y",strtotime($shipment->created_at))}} </td>
							    <td>{{count($shipment->products)}}</td>
							    <td>{{$shipment->cost}}</td>
							    <td><a href="{{route('admin.shipping',['id'=>$shipment->id])}}" class="btn btn-raised btn-primary btn-sm">VIEW SHIPPING</a></td>
					  		</tr>
					  	@endforeach
					  		
					</tbody>
				</table>
      		</div>
      		<a href="{{route('admin.newShipping')}}" class="btn btn-success btn-fab add-fab-btn"><i class="material-icons">add</i></a>
      </div>


    </div>  
@endsection
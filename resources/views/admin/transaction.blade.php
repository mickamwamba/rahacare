@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Transaction ID - #{{$transaction->id}} </h3>
      </div>

      <div class="row">
      	<div class="summary-wrapper">
      		<div class="col-sm-6">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 			 <div class="panel-heading">Transaction details</div>
						  <ul class="list-group">
						    <li class="list-group-item list-item-custom">Transaction #: <span class="pull-right">{{$transaction->id}}</span></li>
						    <li class="list-group-item list-item-custom">Amount:<span class="pull-right">{{$transaction->amount}}</span></li>						    
						    <li class="list-group-item list-item-custom">Transaction date: <span class="pull-right">{{date("D,d-M-Y",strtotime($transaction->created_at))}}</span></li>
						    <li class="list-group-item list-item-custom">Payment method:<span class="pull-right">{{$transaction->method->name}}</span></li>
						    <li class="list-group-item list-item-custom">Status:<span class="pull-right">{{$transaction->status}}</span></li>
						  </ul>
				</div>
      		</div>

      		<div class="col-sm-6">
			    <div class="panel panel-default">
			 		 <!-- Default panel contents -->
			 			<div class="panel-heading">Order details</div>
						  <ul class="list-group">
						    <li class="list-group-item list-item-custom">Order #: <span class="pull-right">{{$transaction->order->id}}</span></li>
						    <li class="list-group-item list-item-custom">Order Value: <span class="pull-right">{{$transaction->order->value}}/=</span></li>
						    <li class="list-group-item list-item-custom">Order date: <span class="pull-right">{{date("D,d-M-Y",strtotime($transaction->created_at))}} </span></li>
						    <li class="list-group-item list-item-custom">Status:<span class="pull-right">{{$transaction->order->status}}</span></li>
						  </ul>
						<div class="panel-foo"><a href="{{route('admin.order',['id'=>$transaction->order->id])}}" class="btn btn-default btn-lg btn-block btn-raised btn-primary">GO TO ORDER</a></div>
				</div>
      		</div>
      	</div>
      </div>
    </div>
@endsection
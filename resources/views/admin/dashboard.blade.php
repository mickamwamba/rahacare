@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Dashboard </h3>
      </div>

      <div class="row summary-details">
      	<div class="">
      		<div class="col-sm-3 detail-wrapper">
      			<span>{{$open_orders}}</span>
      			<p>Open paid orders</p>
      		</div>

      		<div class="col-sm-3 detail-wrapper">
      			<span>{{$current_month_sales}}/=</span>
      			<p>Sales this month</p>
      		</div>

      		<div class="col-sm-3 detail-wrapper">
      			<span>{{$customers}}</span>
      			<p>Total customers</p>
      		</div>
      	</div>
      		

      </div>

      <div class="row">

      		<div class="col-sm-12">
      			<div class="orders-wrapper">
      				<div class="panel panel-default">
						  <!-- Default panel contents -->
						<div class="panel-heading">Latest orders</div>  
						 @if(count($orders)==0)
					      		<div class="content-not-found">
									<p>NO ORDER HISTORY FOUND</p>
								</div>
						 @else 	

		                 <table class="table table-striped table-hover animated fadeIn">
					<thead>
						<tr>
							<th>#</th>
							<th>Order number</th>
							<th>Order Person</th>
							<th>Order value</th>
							<th>Time</th>
							<th>Status</th>
							<th>Payment status</th>
							<th>Actions</th>
					 	</tr>
					</thead>
					<tbody>
					  	@foreach($orders as $order)
					  		<tr>
							    <td></td>
							    <td>{{$order->id}}</td>
							    <td>{{$order->customer->fname." ".$order->customer->sname}}</td>
							    <td>{{$order->value}}</td>
							    <td>{{date("D, d-M-Y H:i")}}</td>
								<td>{{$order->status}}</td>
							    <td> <span class="{{$order->paid?'paid-order':'unpaid-order'}}"> {{$order->paid?'PAID':'NOT PAID'}} </span> </td>								
							    <td><a href="{{route('admin.order',['id'=>$order->id])}}" class="btn btn-raised btn-primary btn-sm">VIEW ORDER</a></td>
					  		</tr>
					  	@endforeach
					  		
					</tbody>
				</table>
				@endif
						<div class="panel-foo"><a href="{{route('admin.orders')}}" class="btn btn-default btn-lg btn-block btn-raised btn-primary">View all orders</a></div>
					</div>
      			</div>
      		</div>

      </div>


      <div class="row">

      		<div class="col-sm-6">
      			<div class="transactions-wrapper">
      				<div class="panel panel-default">
						  <!-- Default panel contents -->
						<div class="panel-heading">Latest transactions</div>  	
		                  @if(count($transactions)==0)
					      		<div class="content-not-found">
									<p>NO ORDER HISTORY FOUND</p>
								</div>
						 @else 	
		                 <table class="table table-striped table-hover animated fadeIn">
							  <thead>
									<tr>
									    <th>Transaction ID</th>
									     <th>Amount</th>
									    <th>Method</th>
									    <th>Time</th>
									    
							 		 </tr>
							  </thead>
							  <tbody>
							  	@foreach($transactions as $transaction)
							  		<tr>
									    <td>#{{$transaction->id}}</td>
									    <td>{{$transaction->amount}}</td>
									    <td>{{$transaction->method->name}}</td>
									    <td>{{date("D,d-M-Y H:i", strtotime($transaction->created_at))}}</td>
							  		</tr>
							  	@endforeach
							  		
							  </tbody>
						</table>
						@endif
						<div class="panel-foo"><a href="{{route('admin.transactions')}}" class="btn btn-default btn-lg btn-block btn-raised btn-default">View all transaction</a></div>
					</div>
      			</div>
      		</div>

      		<div class="col-sm-6">
      			<div class="reports-wrapper">
      				<div class="panel panel-default">
						  <!-- Default panel contents -->
						<div class="panel-heading">Report Data <span></span></div>  	
		                  <table class="table table-striped table-hover animated fadeIn">
							  <thead>
									<tr>
									    <th>Day</th>
									    <th class="pull-right">Order value</th>
									    
							 		 </tr>
							  </thead>
							  <tbody>
							  	<tr><th>Today</th><td class="pull-right">{{$orders_summary['today']}}</td></tr>
							  	<tr><th>Yesterday</th> <td class="pull-right">{{$orders_summary['yesterday']}}</td></tr>
							  	<tr><th>This week</th><td class="pull-right">{{$orders_summary['this_week']}}</td></tr>
							  	<tr><th>Last week</th> <td class="pull-right">{{$orders_summary['last_week']}}</td></tr>
							  	<tr><th>This month</th><td class="pull-right">{{$orders_summary['this_month']}}</td></tr>
							  	<!-- <tr><th>Last month</th><td class="pull-right">{{$orders_summary['last_month']}}</td></tr> -->
													  		
							  </tbody>
						</table>
						<div class="panel-foo"><a href="{{route('admin.reports')}}" class="btn btn-default btn-lg btn-block btn-raised btn-default">View all reports</a></div>
					</div>
      			</div>
      		</div>

      </div>
   </div>

@endsection

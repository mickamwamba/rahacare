@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Transactions {{!empty($customer)?"- ".$customer->fname." ".$customer->sname:''}}</h3>
      </div>

      <div class="row">
      	<form method="POST" action="{{route('admin.filterTransactions')}}">
      	<div class="filters">
      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <input placeholder="--From date--" name= "from_date" type="text" id="date-start" value="{{!empty($from)?$from:''}}" class="form-control datepicker end-date">
           		 </div> 
      		</div>

      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <input placeholder="--To date--" name= "to_date" type="text" id="date-end" value ="{{!empty($to)?$to:''}}"  class="form-control datepicker end-date">
           		 </div> 
      		</div>

      		<div class="col-sm-3">
      			<div class="form-group">
				    <label for="select111" class="col-md-2 control-label">Method</label>
				    <div class="col-md-10">
				        <select id="select111" class="form-control" name="method">
				        <option></option>
				        <option value="1" {{(!empty($method)&&$method==1?'selected="selected"':'')}}>PESAPAL</option>
				        <option value="2" {{(!empty($method)&&$method==2?'selected="selected"':'')}}>MASTERCARD</option>
				        <option value="3" {{(!empty($method)&&$method==3?'selected="selected"':'')}}>MOBILE</option>
				        </select>
				    </div>
				</div>
      		</div>

      		  {{ csrf_field() }}
      		<div class="col-sm-3">
      			<div class="form-group form-elements form-group-custom label-floating co-md-12">
               		 <button class="btn btn-raised btn-default btn-xs" type="submit"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span>FILTER</button>
           		 </div> 
      		</div>
 
      	</div>	
   		</form>
      </div>
        @if(!empty($from))
 			<div class="search-result-text"><h4> Transactions from {{$from}} to {{$to}}</h4></div>     		
      	@endif
      <div class="row">
      		<div class="listing-wrapper">
      		  @if(count($transactions)==0)
		      		<div class="content-not-found">
						<p>NO TRANSACTION HISTORY FOUND</p>
					</div>
			  @else 
      			<table class="table table-striped table-hover animated fadeIn">
					<thead>
						<tr>
							<th>#</th>
							<th>Transaction ID</th>
							<th>Amout</th>
							<th>Method</th>
							<th>Time</th>
							<th>Status</th>
							<th>Actions</th>
					 	</tr>
					</thead>
					<tbody>
					  	@foreach($transactions as $transaction)
					  		<tr>
							    <td></td>
							    <td>{{$transaction->id}}</td>
							    <td>{{$transaction->amount}}</td>
							    <td>{{$transaction->method->name}}</td>
							    <td>{{date("D,d-M-Y H:i",strtotime($transaction->created_at))}}</td>
							    <td>{{$transaction->status}}</td>
							    <td><a href="{{route('admin.transaction',['id'=>$transaction->id])}}" class="btn btn-raised btn-primary btn-sm">VIEW TRANSACTION</a></td>
					  		</tr>
					  	@endforeach
					  		
					</tbody>
				</table>
				{{$transactions->render()}}
				@endif
      		</div>
      </div>

    </div>  
@endsection
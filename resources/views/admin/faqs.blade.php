@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>FAQs </h3>
      </div>


	   <div class="row">
	      	<div class="listing-wrapper col-md-10 col-md-offset-1">

	      		<div class="list-group">

		      		@if(count($faqs)==0)
		      		<div class="content-not-found">
						<p>NO FAQ FOUND</p>
					</div>

					@else 
					@foreach($faqs as $faq)
					  <div class="list-group-item">
					    <div class="row-content">
					      <h4 class="list-group-item-heading"><a href="{{route('admin.faq',['id'=>$faq->id])}}">{{$faq->question}}</a></h4>
					      <p class="list-group-item-text" style="text-align: justify;">  {{strip_tags(substr($faq->answer,0,300))}}</p>
					    </div>
					  </div>
					  <div class="list-group-separator"></div>
					@endforeach
					<div class="list-pagination col-md-offset-1">{{$faqs->render()}}</div> 
		      		@endif
				</div>

				<a href="{{route('admin.addFAQ')}}" class="btn btn-success btn-fab add-fab-btn">
					<i class="material-icons">add</i>
				</a>
	   		</div>
	      
	   </div>
	</div>

@endsection
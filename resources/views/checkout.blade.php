@extends('includes.master')

@section('content')
    @include('includes.checkout_navbar')
    <section class="container-fluid">
            @if (session('msg'))
                {{session('msg')}}
            @endif
        <div class="row">
            {{-- payment processes --}}
            <div class="col-md-8 payment-section-margin">
                <div class="tab">
                    <div class="container-fluid">
                        <div class="tab-group row">
                            <button class="tablinks col-md-4" onclick="openTab(event, 'customer')" id="defaultOpen">01 CUSTOMER INFO</button>
                            {{--  <button class="tablinks col-md-4" onclick="openTab(event, 'delivery')" id="defaultOpen">02 DELIVERY INFO</button>  --}}
                            {{--  <button class="tablinks col-md-4" onclick="openTab(event, 'payment')">02 PAYMENT SECTION  --}}
                            </button>
                        </div>
                    </div>

                 {{-- customer info tab--}}
                    <div id="customer" class="tabcontent">
                        <div class="row">
                            <div class="col-md-6">
                                {{--  <p>customer information</p>  --}}
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('auth.logout')}}"><p style="font-size: 12px;text-align: right">Login as a different user?</p></a>
                            </div>
                        </div>
                        <form action="/complete_order" method="POST">
                            <div class="form-group">
                                <input class="form-control" placeholder="Email" type="email" value="{{$user->email}}" required>
                            </div>
                            <p>Shipping address</p>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input class="form-control" placeholder="First name" type="text" value="{{$user->fname}}" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <input class="form-control" placeholder="Last name" type="text" value="{{$user->sname}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Company (optional)" type="text">
                            </div>
                            <div class="row">
                                <div class="col-md-8 form-group">
                                    <input class="form-control" placeholder="Address (Optional)" type="text" >
                                </div>
                                {{--  <div class="col-md-4 form-group">
                                    <input class="form-control" placeholder="Apt, suite, etc. (optional)" type="text">
                                </div>  --}}
                            </div>
                            <div class="form-group">
                                <select name="region_id" id="user_region" class="form-control" onChange="calculateDeliveryCost()">
                                    @foreach($regions as $region)
                                <option value="{{$region->id}}" {{$region->id==$user->region->id?'selected="selected"':''}}>{{$region->name}}</option>
                                    @endforeach
                                </select>
                                {{--  <input class="form-control" placeholder="City" type="text" value="{{$user->region?$user->region->name:''}}" required>  --}}
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input class="form-control" placeholder="Country" value="{{$user->region?$user->region->country->name:''}}" type="text">
                                </div>
                                <div class="col-md-6 form-group">
                                    <input class="form-control" placeholder="Postal code" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Phone" value="{{$user->phone}}" name="phone" type="text" required>
                            </div>

                          <input type="hidden" name="order_id" value="{{$order->id}}">
                          <input type="hidden" name="user_id" value="{{$user->id}}">
                            {{--  <button type="submit" id="btnAddToBasket">Continue to payment</button>  --}}

                    </div>

                    {{-- delivery info tab --}}
                    <div id="delivery" class="tabcontent">
                        <h3>delivery</h3>
                        <p>delivery is also important.</p>
                    </div>
                </div>
            </div>

            {{-- summery section --}}
            <div class="col-md-4 cart-summery" style="color: black;font-weight: bold">
                    <div class="allMarginContainer">
                        <p id="header-cart-summery">Summary</p>
                        <div class="row" id="fiftyPaddingTop">
                            <div class="col-md-6">
                                <p>Subtotal</p>
                            </div>
                            <div class="col-md-6">
                                <p id="subtotal">Tsh</p>
                            </div>
                        </div>
                        <div class="row" id="fiftyPaddingTop" style="border-bottom: 2px solid lightgray; padding-bottom: 20px">
                            <div class="col-md-6">
                                <p>Delivery</p>
                            </div>
                            <div class="col-md-6">
                                <p id="delivery_cost">Tsh 25,000.00</p>
                            </div>
                        </div>
                        <div class="row" id="fiftyPaddingTop">
                            <div class="col-md-6">
                                <p>Total</p>
                            </div>
                            <div class="col-md-6">
                                <p id="totalcost">Tsh 250,000.00</p>
                            </div>
                        </div>
                        {{--  <a id="btnCheckOut" href=""></a>  --}}
                            {{ csrf_field() }}
                            <input type="hidden" name="order_id" value="{{$order->id}}">
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <input type="hidden" id="user_delivery_cost" name="delivery_cost" value="{{$delivery_cost}}">
                            
                        
                            <button type="submit" id="btnCheckOut"> <span>CONTINUE TO PAYMENT</span> </button>

                        </form>
                    </div>
                </div>
        </div>
    </section>
    
@endsection

<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">

  $(document).ready(function () {


        // var delivery_cost = 10000;
        var delivery_cost = '<?php echo $delivery_cost;?>'
        // alert(delivery_cost);

       $('#subtotal').html(TotalOrderCost());
       $('#delivery_cost').html(delivery_cost);
       $('#totalcost').html(TotalOrderCost()+parseInt(delivery_cost));


        // Create a Stripe client
var stripe = Stripe('pk_test_VV7Rs43i8AM7WY4G8JzRAR75');

// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server
      stripeTokenHandler(result.token);
    }
  });
});

    });
    function TotalOrderCost()
    {

        var cart = JSON.parse(localStorage.getItem("cart")); 
        var total = 0;
        for(var i=0; i<cart.length; i++)
        {
            total +=cart[i].price * cart[i].quantity;
        }
        return total;
    }

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}

function calculateDeliveryCost()
{   
    var delivery_cost_dar = 8000;
    var delivery_cost_out_of_dar = 12000;
    var delivery_cost_international = 25000;
     
    var region_id = $('#user_region').val();
    if(region_id==2){
       $('#delivery_cost').html(delivery_cost_dar);        
    }
    else if(region_id==32){
       $('#delivery_cost').html(delivery_cost_international);
    }
    else 
    $('#delivery_cost').html(delivery_cost_out_of_dar);

    $('#user_delivery_cost').val(parseInt($('#delivery_cost').html()));
    $('#totalcost').html(TotalOrderCost()+parseInt($('#delivery_cost').html()));
    
}

  </script>


@extends('includes.master')
@section('title','Cart')
@section('content')
    @include('includes.white_bg_navbar')
    <section class="container-fluid">
        <div class="row">
            {{-- users cart products --}}
            <div class="col-md-8 allMarginContainer">
                <p style="font-weight: bold;font-size: 20px;color: black">Your Cart <span id="cart_count"></span> </p>
                {{-- one product row --}}
                <div id="empty_cart">

                </div>
                <div id="cart_contents">
                  
                </div>
               
            </div>

            {{-- summery section --}}
            <div class="col-md-4 cart-summery" style="color: black;font-weight: bold">
                <div class="allMarginContainer">
                    <p id="header-cart-summery">Summary</p>
                    <div class="row cost-row" id="fiftyPaddingTo">
                        <div class="col-md-6">
                            <p>Subtotal</p>
                        </div>
                        <div class="col-md-6">
                            <p id="subtotal">Tsh</p>
                        </div>
                    </div>
                    <div class="row" id="fiftyPaddingTop" style="border-bottom: 2px solid lightgray; padding-bottom: 20px">
                        {{--  <div class="col-md-6">
                            <p>Delivery</p>
                        </div>
                        <div class="col-md-6">
                            <p id="delivery_cost">Tsh</p>
                        </div>  --}}
                    </div>
                    <div class="row cost-row" id="fiftyPaddingTo">
                        <div class="col-md-6">
                            <p>Total</p>
                        </div>
                        <div class="col-md-6">
                            <p id="totalcost">Tsh</p>
                        </div>
                    </div>
                    <div class="row delivery_disclaimer">
                        <p>Total cost is subjected to change, depending on delivery cost for your location</p>
                    </div>
                    {{--  <a id="btnCheckOut" href=""></a>  --}}
                    <button id="btnCheckOut" class=""> <span>CONFIRM ORDER</span> </button>
                </div>
            </div>
        </div>
    </section>
@endsection
<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>

<script type="text/javascript">
     $(document).ready(function(){
        if(localStorage.getItem("cart")==null||JSON.parse(localStorage.getItem("cart")).length==0){
          $('#btnCheckOut').css({
            display: 'none',
        });

        $('#empty_cart').html('<div class="" style="text-align:center"><p style="margin-bottom:23px; color:#b96161">Your cart is empty!</p><a id="shopNow" style="border:solid #eee 2px;color:#000" href="{{route('raha.products')}}"><span>SHOP NOW</span></a></div>');

        }
         var delivery_cost = 10000;

        $('#subtotal').html(TotalOrderCost());
        $('#delivery_cost').html(delivery_cost);
        $('#totalcost').html(TotalOrderCost());


        var el= document.getElementById("cart_contents");
        var cart = JSON.parse(localStorage.getItem("cart"));
 
        $('#cart_count').html("<strong>( "+cart.length+" )</strong>");
        // alert(cart[0].product_name);
        el.innerHTML = '';
        for(var i = 0; i<cart.length; i++){
            if(cart[i].product_option==null) cart[i].product_option = '';
            el.innerHTML += '<article style="border-bottom: 1px solid rgba(128,128,128,0.27); padding-top: 10px;margin-top: 20px">\
                    <div class="row">\
                        {{-- product image + product descript --}}\
                        <div class="col-md-6">\
                            <div class="row">\
                                <div>\
                                    <img src="'+cart[i].product.media[0].media+'" width="100px" height="130px">\
                                </div>\
                                <div style="margin-left: 20px">\
                                    <p style="font-weight: bold;font-size: 20px;color: black">'+cart[i].product_name+'</p>\
                                    <p>'+cart[i].category+'</p><p>'+cart[i].product_option+'</p></p>\
                                </div>\
                            </div>\
                        </div>\
                        {{-- product quantity and price --}}\
                        <div class="col-md-6 row">\
                            <div class="col-md-4">\
                                <div class="row">\
                                    <div>\
                                        <input type="text" id="current_item_no" value="'+cart[i].quantity+'" readonly="readonly" size="4">\
                                    </div>\
                                    <div>\
                                        <img src="/images/web_assets/increase_quantity.png" width="25px" height="25px" id="increase-item"><br>\
                                        <img src="/images/web_assets/decrease_quantity.png" width="25px" height="25px" id="decrease-item">\
                                    </div>\
                                </div>\
                            </div>\
                              <div class="col-md-2">\
                                <p>\
                                    <span>'+cart[i].size+'</span></span>\
                                </p>\
                            </div>\
                            <div class="col-md-6">\
                                <p>\
                                    <span>Tsh '+cart[i].quantity * cart[i].price+'</span>\
                                    <img align="right" src="/images/web_assets/cross.png" width="25px" height="25px" onclick="removeFromCart('+cart[i].product_id+')">\
                                </p>\
                            </div>\
                        </div>\
                    </div>\
                </article>';
        }
        

    $('#btnCheckOut').click(function(){
        
        var method = "GET";
        var path = "/confirm_order";
        var user_cart = localStorage.getItem("cart"); 
      
        var hiddenField;
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);

        hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", 'cart');  
        hiddenField.setAttribute("value", user_cart);
        form.appendChild(hiddenField);
        
        userField = document.createElement("input");
        userField.setAttribute("type","hidden");
        userField.setAttribute("name","user_id");
        userField.setAttribute("value",2);
        form.appendChild(userField);

         tokenField = document.createElement("input");
         tokenField.setAttribute("type", "hidden");
         tokenField.setAttribute("name", '_token');  
         tokenField.setAttribute("value", "<?=csrf_token()?>");
         form.appendChild(tokenField);

      

       // console.log(cart);   
       document.body.appendChild(form);
       form.submit();


    });

    function TotalOrderCost()
    {

        var cart = JSON.parse(localStorage.getItem("cart")); 
        var total = 0;
        for(var i=0; i<cart.length; i++)
        {
            total +=cart[i].price * cart[i].quantity;
        }
        return total;
    }

    });

function removeFromCart(product_id)
    { 
        var cart = JSON.parse(localStorage.getItem("cart")); 
        console.log(cart);
        index = cart.findIndex(x => x.product_id==product_id);
        cart.splice(index, 1);

        localStorage.setItem("cart",JSON.stringify(cart));
        location.reload();
      //  if(localStorage.getItem("cart").length==0) localStorage.removeItem("cart");
    }
</script>
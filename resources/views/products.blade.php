@extends('includes.master')
@section('title','Raha Products')
@section('content')
    @include('includes.black_bg_navbar')
    
    
       <div class="tab">
            <div class="container-fluid">
                <div class="tab-group row raha-tabs">
                    <button class="tablinks col-md-4" onclick="openTab(event, 'product_list_info')" id="defaultOpen">BREAST FORMS</button>
                    <button class="tablinks col-md-4" onclick="openTab(event, 'accessories_list_info')" >CARE & ACCESSORIES</button>
                </div>
            </div>

                {{-- product listing tab--}}
            <div id="product_list_info" class="tabcontent">
                   {{-- first section --}}
                <section class="product_pink_page">
                <div class="container" id="fiftyPaddingTop">
                    <h1 id="products-welcome">light-weight</h1>
                    <h1 id="products-welcome">breast forms</h1>
                </div>
                    <div class="container">
                        {{--  <ul id="product_list_info">
                            <li>BREASTS FORM</li>
                            <li>CARE & ACCESSORIES</li>
                        </ul>  --}}
                    

                    </div>
                
                </section>

                  {{-- product section --}}
                <section class="allMarginContainer container-fluid">
                    <div class="row">
                        {{-- one product item --}}
                        @foreach($products as $product) 
                            @if($product->category_id==1)
                            <article class="col-lg-3 col-sm-4 col-xs-6">
                            
                                    <div class="text-center raha-product">
                                        <a href="/product/{{$product->id}}" id="product-item-link">
                                            <img class="thumbnail img-responsive" src="{{$product->media->first()->media}}" width="100%" height="320px">
                                            <p style="font-weight: bold;">{{$product->name}}</p>
                                            <p>Tshs. {{count($product->shipment_products)>0?$product->shipment_products[0]->pivot->unit_selling_price:''}}</p>
                                        </a>
                                        <div class="rating">
                                            <span>☆ </span><span>☆ </span><span>☆ </span><span>☆ </span><span>☆</span>
                                        </div>
                                        {{--<fieldset class="rating">--}}
                                        {{--<input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="very good">5 stars</label>--}}
                                        {{--<input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="good">4 stars</label>--}}
                                        {{--<input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="okay">3 stars</label>--}}
                                        {{--<input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="bad">2 stars</label>--}}
                                        {{--<input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="very bad">1 star</label>--}}
                                        {{--</fieldset>--}}
                                        <p class="addToCartButton"><a href="#" style="text-decoration: none">ADD TO CART</a></p>
                                    </div>
                            
                            </article>
                            @endif
                        @endforeach 
                    </div>
                </section>                
            </div>

                 {{--care and accessories listing--}}
            <div id="accessories_list_info" class="tabcontent">
                   {{-- first section --}}
                <section class="product_pink_page">
                <div class="container" id="fiftyPaddingTop">
                    <h1 id="products-welcome">Care & Accesories</h1>
                    <h1 id="products-welcome">special for you</h1>
                </div>
                    <div class="container">
                        {{--  <ul id="product_list_info">
                            <li>BREASTS FORM</li>
                            <li>CARE & ACCESSORIES</li>
                        </ul>  --}}
                    

                    </div>
                
                </section>

                  {{-- product section --}}
                <section class="allMarginContainer container-fluid">
                    <div class="row">
                        {{-- one product item --}}
                       @foreach($products as $product) 
                            @if($product->category_id==2)
                            <article class="col-lg-3 col-sm-4 col-xs-6">
                            
                                    <div class="text-center raha-product">
                                        <a href="/product/{{$product->id}}" id="product-item-link">
                                            <img class="thumbnail img-responsive" src="{{$product->media->first()->media}}" width="100%" height="320px">
                                            <p style="font-weight: bold;">{{$product->name}}</p>
                                            <p>Tshs. {{count($product->shipment_products)>0?$product->shipment_products[0]->pivot->unit_selling_price:''}}</p>
                                        </a>
                                        <div class="rating">
                                            <span>☆ </span><span>☆ </span><span>☆ </span><span>☆ </span><span>☆</span>
                                        </div>
                                        {{--<fieldset class="rating">--}}
                                        {{--<input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="very good">5 stars</label>--}}
                                        {{--<input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="good">4 stars</label>--}}
                                        {{--<input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="okay">3 stars</label>--}}
                                        {{--<input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="bad">2 stars</label>--}}
                                        {{--<input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="very bad">1 star</label>--}}
                                        {{--</fieldset>--}}
                                        <p class="addToCartButton"><a href="#" style="text-decoration: none">ADD TO CART</a></p>
                                    </div>
                            
                            </article>
                            @endif
                        @endforeach 
                    </div>
                </section>                
            </div>

        </div>

@endsection
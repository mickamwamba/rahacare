{{--  @extends('layouts.user_login')  --}}
@extends('includes.master')
@section('content')
@include('includes.login_navbar')
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
                <div class="panel panel-default" style="padding: 15px; margin-top: 30px;">
                <div class="panel-heading login-title">Welcome to Raha Care</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-12 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-login-custom">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                    <hr>

                    <a href="{{route('signUp')}}">Sign up now</a> <br> 
                    
                   
                    <center><h4>OR</h4></center>
                    <a href="/auth/facebook" class="btn btn-lg btn-facebook btn-block">Connect with Facebook</a>

                    <a href="/auth/google" class="btn btn-lg btn-google btn-block">Connect with Google</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

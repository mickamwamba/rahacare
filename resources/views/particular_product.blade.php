@extends('includes.master')
@section('title','Product')
@section('content')
    @include('includes.white_bg_navbar')

    {{-- first section --}}
    <section class="container product-info">
        <div class="row">
            <div class="col-xs-6 col-md-6 product-img">
                <img class="img-responsive" src="{{$product->media->first()->media}}" width="400px" height="450px">
            </div>
            <div class="col-xs-6 col-md-6 allMarginContainer">
                <div class="rating">
                    <span>☆ </span><span>☆ </span><span>☆ </span><span>☆ </span><span>☆</span>
                </div>
                <p id="bold-fifty-black">{{$product->name}}</p>
                <p>Our best selling light-weight breast form for ladies of all ages</p>

                <div>
                    <p>SELECT SIZE</p>
                    <p class="product-sizes" id="product-sizes" data-toggle="tooltip" data-placement="right" title="Please select size">
                        <span id="circle_no" class="size-selector" >41</span>
                        <span id="circle_no" class="size-selector" >42</span>
                        <span id="circle_no" class="size-selector" >43</span>
                        <span id="circle_no" class="size-selector" >44</span>
                        <span id="circle_no" class="size-selector" >45</span>
                        <a href="#">Size calculator</a>
                    </p>
                </div>
                 <div>QUANTITY</div>
                <div class="quantity-selector">
                
                    <div class="row">
                                    <div>
                                        <input type="text" id="current_item_no" value="1" readonly="readonly" size="4">
                                    </div>
                                    <div>
                                        <img src="/images/web_assets/increase_quantity.png" width="25px" height="25px" id="increase-item"><br>
                                        <img src="/images/web_assets/decrease_quantity.png" width="25px" height="25px" id="decrease-item">
                                    </div>
                    </div> 
                    {{--  <div>  <input type="text" id="current_item_no" value="1" readonly="readonly" size="4"> <span>QUANTITY</span></div>  --}}
                </div>
                <div>
                @if(count($product->options)>0)
                <div class="color-selector" data-toggle="tooltip" data-placement="right" title="Please select Color">COLOR</div>
                @foreach($product->options as $opt)
                     <label class="radio-inline"><input type="radio" style="color:gray;margin-right:5px;border-color:red"  value="{{$opt->id}}" name="product_option_id">{{$opt->option->value}}</label>
                @endforeach
                </div>
                @endif
                <h1 id="fiftyPaddingTop" class="header-one">Tshs.{{count($product->shipment_products)>0?$product->shipment_products[0]->pivot->unit_selling_price:''}} *</h1>
                <p>
                    <button type="button" id="btnAddToBasket">ADD TO CART</button>


                    <div id="alert" class ="go_to_checkout" style="display:none"> <a href="/shopping/cart">GO TO CHECKOUT</a></div>
                </p>
            </div>
        </div>
    </section>

    {{-- second section --}}
    <section class="container allMarginContainer">
        <div class="row">
            <div class="text-center col-md-4" id="rating-right-border">
                <p style="font-size: 80px; color: black">4.7</p>
                <div class="rating">
                    <span>☆ </span><span>☆ </span><span>☆ </span><span>☆ </span><span>☆</span>
                </div>
                <p>128 reviews</p>
            </div>
            <div class="col-md-8">
                <h3>Description</h3>
                <p>
                    {{$product->description}}
                </p>
            </div>
        </div>
    </section>
@endsection
<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>

<script type="text/javascript">
var user_size;
  $(document).ready(function(){
    $("[data-toggle=tooltip").tooltip();
    $('.size-selector').click(function() {
  user_size = $(this).html(); 
  //alert(user_size);
  $(this).parent().find('.size-selector').css('background-color', '#d3d3d3');
  $(this).parent().find('.size-selector').css('color','#000');
  $(this).parent().find('.size-selector').removeClass('activeSize');
  $(this).css('background-color', '#f6cccb');
  $(this).css('color', '#ffffff');
  $(this).addClass('activeSize');
});


    $('#btnAddToBasket').click( function() {
        
    var existingEntries = JSON.parse(localStorage.getItem("cart"));
    if(existingEntries == null) existingEntries = [];

    var options_count = <?php echo count($product->options);?>;
    // alert(options_count);
    var product = <?php echo $product;?>;
     var product_id = <?php echo $product->id;?>;
     var product_name = '<?php echo $product->name;?>';
     var price = <?php echo count($product->shipment_products)>0?$product->shipment_products[0]->pivot->unit_selling_price:'';?>;
     var shipment_product_id = <?php echo count($product->shipment_products)>0?$product->shipment_products[0]->id:'';?>;
     var category = '<?php echo $product->category->name;?>';
     var product_option_id = options_count>0?$('input[name=product_option_id]:checked').val():null;
     var product_option = $("input:radio:checked").parent()[0]?$("input:radio:checked").parent()[0].innerText:null;
    var productSize=$('.activeSize').text();
    //checks if productsize is not selected
    if(!productSize){
   $('#product-sizes').tooltip('show');     
    }

    if(!product_option&&options_count>0){
   $('.color-selector').tooltip('show');  
   console.log(product_option);   
    }



    if(ProductExistInCart(product_id)){
        showProductExistMessage();
        return false;
    }

    if(productSize!=null){
    var quantity = $('#current_item_no').val();
    var size = user_size;
   // alert(user_size);
    var entry = {
        "product_id": product_id,
        "product_name": product_name,
        "quantity": quantity,
        "size":size,
        "price":price,
        "product":product,
        "product_option_id":product_option_id,
        "product_option":product_option,
        "category":category,
        "shipment_product_id":shipment_product_id,
    };
    localStorage.setItem("entry", JSON.stringify(entry));
    existingEntries.push(entry);
    localStorage.setItem("cart", JSON.stringify(existingEntries));

    showCheckoutMessage();
}
    });


});

function ProductExistInCart(product_id){
     var existingEntries = JSON.parse(localStorage.getItem("cart"));
     if(existingEntries == null) existingEntries = [];
     var found = false;    
     for(var i=0; i<existingEntries.length; i++)
     {
       if(product_id == existingEntries[i].product_id)
        {
            found=true;
            break;
        }
     }
    if(found) return true;
    else return false;
}

function showCheckoutMessage()
{  
    Snackbar.show({text: 'Proceed to checkout',
actionText: 'PROCEED',
onActionClick: function(){
     window.location = "{{ route('raha.cart') }}";
},

}); 

    return false;
}

function showProductExistMessage()
{  
    Snackbar.show({text: 'Product Exists in Cart',
actionText: 'PROCEED',
onActionClick: function(){
    window.location = "{{ route('raha.cart') }}";
},

}); 

    return false;
}

//function getSize(value)  $('#alert').css('display', 'block'); $('#alert').fadeIn().delay(5000).fadeOut();
//{  
  //  user_size = value;
//}

</script>
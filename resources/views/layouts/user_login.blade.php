<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
  
    <meta name="theme-color" content="#ffffff">

    <title>RahaCare</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/bootstrap-material-design.min.css')}}">
    <link rel="stylesheet" href="{{url('css/bootstrap-tagsinput.css')}}">
    <link rel="stylesheet" href="{{url('css/jasny-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/helper.css')}}">
    <link rel="stylesheet" href="{{url('css/navmenu.css')}}">
    <link rel="stylesheet" href="{{url('css/main.css')}}">
    <link rel="stylesheet" href="{{url('css/ripples.min.css')}}">
    <link rel="stylesheet" href="{{url('css/animate.css')}}">
    <link rel="stylesheet" href="{{url('css/jquery.dropdown.css')}}">
    <link rel="stylesheet" href="{{url('css/bootstrap-material-datetimepicker.css')}}">
    <link rel="stylesheet" href="{{url('css/tabs.css')}}">
    <link rel="stylesheet" href="{{url('css/dashboard.css')}}">
    <link rel="stylesheet" href="{{url('css/blog.css')}}">
    <link rel="stylesheet" href="{{url('css/shippings.css')}}">

  </head>
  <body>
    {{--  <div class="navmenu navmenu-default navmenu-side navmenu-fixed-left offcanvas-sm agri-menu">
      <!--<a class="navmenu-brand visible-md visible-lg" href="#">Agri Salama Info Admin</a>-->
      <div class="user-header" data-toggle="modal" data-target="#accountModal">
        <div class="information">
          <div class="avatar" style="background-image:url('http://www.valuetime.co.in/images/change-user.png')"></div>
     <!--    @if(Auth::user())
          <p>{{Auth::user()->fname}}  {{Auth::user()->sname}}</p>
          @endif -->
        </div>
      </div>  --}}
    {{--  <ul id="main-menu" class="nav navmenu-nav">
      <li class="withripple active"><a href="/admin/dashboard"><i class="fa fa-dashboard menu-icons"></i>Dashboard</a></li>
      <li><a class="withripple" href="/admin/orders"> <i class="fa fa-shopping-basket menu-icons"></i> Orders</a></li>
      <li><a class="withripple" href="/admin/transactions"><i class="fa fa-exchange menu-icons"></i> Transactions</a></li>
      <li><a class="withripple" href="/admin/shippings"><i class=" fa fa-sign-in menu-icons"></i> Shippings</a></li>      
      <li><a class="withripple" href="/admin/products"><i class="fa fa-cubes menu-icons"></i> Products</a></li>
       <li><a class="withripple" href="/admin/reports"><i class="fa fa-bar-chart menu-icons"></i> Reports</a></li>
     <li><a class="withripple" href="/admin/customers"><i class=" fa fa-users menu-icons"></i> Customers</a></li>
      <li><a class="withripple" href="/admin/blog"><i class="fa fa-newspaper-o menu-icons"></i>Blog</a></li>
      <li><a class="withripple" href="/admin/faqs"><i class="fa fa-question-circle-o menu-icons"></i>FAQs</a></li>
      <li><a class="withripple" href="/logout"><i class="fa fa-reply menu-icons"></i> Log out</a></li>
    </ul>  --}}
  {{--  </div>

  <div class="navbar navbar-default navbar-custom navbar-fixed-top hidden-md hidden-lg ">
    <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
    <a href="#"><i class="fa fa-bars menu-btn" aria-hidden="true"></i></a>

    <div class="navbar navbar-default navbar-custom navbar-fixed-top hidden-md hidden-lg ">
      <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
      <a href="#"><i class="fa fa-bars menu-btn" aria-hidden="true"></i></a>
      </button>
      <a class="navbar-brand" href="#">RahaCare</a>
    </div>
    <div class="toolbar hidden-sm hidden-xs">
      <div class="toolbar-wrapper">
        <img src="{{url('images/logo.png')}}" alt="">
        <p class="toolbar-title">RahaCare</p>
        
          @if (session('msg'))
          <div class="feedbackmsg">
    
                  <div class="modal fade bs-example-modal-lg" id="feedbackmsg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content message" style="left: 100px;height: 94px;padding: 39px;text-transform: uppercase;color: #3d2c56;text-align: center;font-size: 17px;">
                 {{ session('msg') }}
              </div>
            </div>
          </div>
              </div>
          @endif
  </div>
    --}}
    </div>
    @yield('content')
     
  <!-- JavaScripts -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
  crossorigin="anonymous"></script>
  <script type="text/javascript" src="{{url('js/lib/jasny-bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/ripples.min.js')}}"></script>
  <script src="https://use.fontawesome.com/9cf45c8a2f.js"></script>
  <script type="text/javascript" src="{{url('js/lib/material.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/materialize.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/snackbar.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/moment.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/bootstrap-material-datetimepicker.js')}}"></script>
  {{-- <script type="text/javascript" src="{{url('js/lib/jquery.dropdown.js')}}"></script> --}}
  <script type="text/javascript" src="{{url('js/lib/bootstrap-tagsinput.min.js')}}"></script>

  <script type="text/javascript">
  $.material.init();
  </script>
  {{--
  <script src="{{ elixir('js/app.js') }}"></script> --}}
  <script type="text/javascript">
  $.material.init();
  $(document).ready(function () {
  // var i = document.location.href.lastIndexOf("/");
  // var currentPHP = document.location.href.substr(i+1);
  // console.log(currentPHP);
  // var url = "{{url('admin')}}/"+currentPHP;
  // console.log(url);
  // $("ul#main-menu li a").removeClass('active');
  // $("ul#main-menu li a[href^='"+url+"']").addClass('active');


  $('#feedbackmsg').modal('show');
  $('#date').bootstrapMaterialDatePicker({
  time: false,
  clearButton: true
  });
  $(".select").dropdown({
  autoinit: "select"
  });
  $('#date-end').bootstrapMaterialDatePicker
  ({
  weekStart: 0, format: 'DD/MM/YYYY HH:mm'
  });
  $('#date-start').bootstrapMaterialDatePicker
  ({
  weekStart: 0, format: 'DD/MM/YYYY HH:mm', shortTime : true
  }).on('change', function(e, date)
  {
  $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
  });
  });
  </script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="apple-touch-icon" sizes="57x57" href="{{url('images/favicons/apple-icon-57x57.png')}}">

<link rel="apple-touch-icon" sizes="60x60" href="{{url('images/favicons/apple-icon-60x60.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{url('images/favicons/apple-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{url('images/favicons/apple-icon-76x76.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{url('images/favicons/apple-icon-114x114.png')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{url('images/favicons/apple-icon-120x120.png')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{url('images/favicons/apple-icon-144x144.png')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{url('images/favicons/apple-icon-152x152.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{url('images/favicons/apple-icon-180x180.png')}}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{url('images/favicons/android-icon-192x192.png')}}">

<link rel="icon" type="image/png" sizes="32x32" href="{{url('images/favicons/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{url('images/favicons/favicon-96x96.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{url('images/favicons/favicon-16x16.png')}}">
<link rel="manifest" href="{{url('images/favicons/manifest.json')}}">
<meta name="theme-color" content="#ffffff">

  <title>Rahacare|Admin</title>

  <!-- Bootstrap core CSS -->

  <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('css/bootstrap-material-design.min.css')}}">
  <link rel="stylesheet" href="{{url('css/helper.css')}}">
  <link rel="stylesheet" href="{{url('css/main.css')}}">

  <link rel="stylesheet" href="{{url('css/ripples.min.css')}}">
  <link rel="stylesheet" href="{{url('css/animate.css')}}">
  <link rel="stylesheet" href="{{url('css/jquery.dropdown.css')}}">
  <link rel="stylesheet" href="{{url('css/bootstrap-material-datetimepicker.css')}}">
  <link rel="stylesheet" href="{{url('css/tabs.css')}}">
  <link rel="stylesheet" href="{{url('css/logIn.css')}}">
</head>

<body>

  @yield('content')

  <!-- JavaScripts -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
    crossorigin="anonymous"></script>
  <script type="text/javascript" src="{{url('js/lib/jasny-bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/ripples.min.js')}}"></script>
  <script src="https://use.fontawesome.com/9cf45c8a2f.js"></script>
  <script type="text/javascript" src="{{url('js/lib/material.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/materialize.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/snackbar.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/moment.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/bootstrap-material-datetimepicker.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/jquery.dropdown.js')}}"></script>
  <script type="text/javascript">
    $.material.init();
  </script>
  {{--
  <script src="{{ elixir('js/app.js') }}"></script> --}}
  <script type="text/javascript">
    $.material.init();
    $(document).ready(function () {
      $('#date').bootstrapMaterialDatePicker({
        time: false,
        clearButton: true
      });
      $(".select").dropdown({
        autoinit: "select"
      });
    $('#date-end').bootstrapMaterialDatePicker
            ({
                weekStart: 0, format: 'DD/MM/YYYY HH:mm'
            });
            $('#date-start').bootstrapMaterialDatePicker
            ({
                weekStart: 0, format: 'DD/MM/YYYY HH:mm', shortTime : true
            }).on('change', function(e, date)
            {
                $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
            });

    });
  </script>
</body>

</html>
<nav class="navbar navbar-expand-lg navbar-light bg-white">

    <a class="nav navbar-brand" href="/">
        <img src="/images/web_assets/logos/raha_black.png" width="120px" height="100px">
    </a>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="nav navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('raha.products') }}">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp; &nbsp; CONTINUE SHOPPING
                </a>
            </li>
        </ul>

        <span class="navbar-text">
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item">
                    <a class="nav-link" href="#">CHECKOUT</a>
                </li>
            </ul>
        </span>
    </div>
</nav>
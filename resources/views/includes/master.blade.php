<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('/bootstrap/css/bootstrap.min.css') }}">

    {{-- font awesome --}}
    <link rel="stylesheet" type="text/css"  href="{{ URL::to('/font-awesome/css/font-awesome.min.css') }}">

    <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="/my_css/main.css">
        <link rel="stylesheet" href="{{url('css/checkout.css')}}">
        


    {{-- montserrat --}}
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    {{--  revised Styles  --}}
    <link rel="stylesheet" href="{{url('css/revisedStyles.css')}}">
     <link rel="stylesheet" href="{{url('css/snackbar.css')}}">

</head>
<body>
<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    {{--  <a href="/">Home</a>  --}}
    <a href="{{ route('raha.products') }}">Products</a>
    <a href="{{ route('raha.blog') }}">Blog</a>
    <a href="#">Contact Us</a>
    {{--  <a href="#">Favourites</a>  --}}
    <a href="{{ route('raha.cart') }}">Cart</a>
</div>
@yield('content')
<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/main.js') }}"></script>
<!-- <script src="https://js.stripe.com/v3/"></script> -->

<script>
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<script type="text/javascript" src="{{url('js/lib/snackbar.js')}}"></script>
<script type="text/javascript" src="{{url('js/lib/notify.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <button class="navbar-toggler" onclick="openNav()" style="border: none; margin-left: 10px">
        <i class="fa fa-bars"></i>
    </button>

    <a class="nav navbar-brand" href="{{ route('home') }}">
        <img src="/images/web_assets/logos/raha_black.png" width="120px" height="100px">
    </a>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="nav navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">HOME <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('raha.products') }}">PRODUCTS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('raha.blog') }}">BLOG</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/#contact-us">CONTACT US</a>

            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('raha.faq')}}">FAQs</a>
            </li>
        </ul>

        <span class="navbar-text">
            <ul class="nav navbar-nav navbar-right">
               
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('raha.cart') }}">
                        <span class="fa fa-cart-arrow-down cart-icon" aria-hidden="true"></span> CART
                        <span id="cart-items-red" style="display:none"></span>
                    </a>
                </li>
                @auth
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span class="fa fa-user" aria-hidden="true"></span> <span class="user-name"> {{Auth::user()->fname}}</span>
                        {{--  <span id="cart-items-white" style="display:none"></span>  --}}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('auth.logout') }}">
                        <span>| LOGOUT</span>
                        {{--  <span id="cart-items-white" style="display:none"></span>  --}}
                    </a>
                </li>

                @else
                <li class="nav-item" style="border:1px solid">
                    <a class="nav-link" href="{{ route('login') }}">
                        <span class="fa fa-use" aria-hidden="true"></span> SIGN IN
                        {{--  <span id="cart-items-white" style="display:none"></span>  --}}
                    </a>
                </li>
                @endauth

            </ul>
        </span>
    </div>
</nav>

<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>

<script type="text/javascript"> 
    $(document).ready(function(){

    var cart = JSON.parse(localStorage.getItem("cart"));
    if((cart.length)>0) {
       $('#cart-items-red').show();

    }
   
    $('#cart-items-red').html(cart.length)
    });

</script>
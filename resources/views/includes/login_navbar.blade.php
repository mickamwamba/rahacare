<nav class="navbar navbar-expand-lg navbar-light bg-white">
        <button class="navbar-toggler" onclick="openNav()" style="border: none; margin-left: 10px">
            <i class="fa fa-bars"></i>
        </button>
    
        <a class="nav navbar-brand navbar-brand-custom" href="{{route('home')}}">
            <img src="/images/web_assets/logos/raha_black.png" width="120px" height="100px">
        </a>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="nav navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('home') }}">HOME <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('raha.products') }}">PRODUCTS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('raha.blog') }}">BLOG</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/#contact-us">CONTACT US</a>

                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('raha.faq')}}">FAQs</a>
                </li>
            </ul>
    
        </div>
    </nav>
    
    <script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
    
    <script type="text/javascript"> 
        $(document).ready(function(){
        var cart = JSON.parse(localStorage.getItem("cart"));
        $('#cart-items').html(cart.length)
        });
    
    </script>
@extends('includes.master')

@section('content')
    @include('includes.checkout_navbar')
    <section class="container-fluid">
            @if (session('msg'))
                {{session('msg')}}
            @endif
        <div class="row">
            {{-- payment processes --}}
            <div class="col-md-12 payment-section-margin">
                <div class="tab">
                    <div class="container-fluid">
                        <div class="tab-group row">
                            <button class="tablinks col-md-4" onclick="openTab(event, 'customer')" id="defaultOpen">PAYMENT OPTIONS</button>
                            {{--  <button class="tablinks col-md-4" onclick="openTab(event, 'delivery')" id="defaultOpen">02 DELIVERY INFO</button>  --}}
                            {{--  <button class="tablinks col-md-4" onclick="openTab(event, 'payment')">02 PAYMENT SECTION  --}}
                            </button>
                        </div>
                    </div>

                 {{-- customer info tab--}}
                    <div id="customer" class="tabcontent">
                        <div class="row">
                            <div class="col-md-6">
                                {{--  <p>customer information</p>  --}}
                            </div>
                            <div class="col-md-6">
                                {{--  <a href=""><p style="font-size: 12px;text-align: right">Login as a different user?</p></a>  --}}
                            </div>
                        </div>
                        {!!$iframe!!}
                    </div>

                </div>
            </div>

        
        </div>
    </section>
    
@endsection

<script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">

  $(document).ready(function () {
        // Create a Stripe client
var stripe = Stripe('pk_test_VV7Rs43i8AM7WY4G8JzRAR75');

// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server
      stripeTokenHandler(result.token);
    }
  });
});

    });

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}

  </script>


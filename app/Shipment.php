<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    //
    public function products()
    {
    	return $this->hasMany('App\ShipmentProduct');
    }
}

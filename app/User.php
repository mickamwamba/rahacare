<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $casts = [
        'settings' => 'array'
    ];
    
    protected $fillable = [
        'fname','sname','phone','role_id','region_id', 'email', 'password','settings'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function region(){
        return $this->belongsTo('App\Region','region_id');
    }
}

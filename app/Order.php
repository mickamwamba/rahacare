<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public function customer()
    {
    	return $this->belongsTo('App\User','user_id');
    }

    public function transactions()
    {
    	return $this->hasMany('App\Transaction');
    }
    
    public function items()
    {
    	return $this->hasMany('App\OrderItem');
    }
}

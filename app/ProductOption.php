<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    //
    protected $table = 'product_options';

    public function option()
    {
        return $this->belongsTo('App\Option','option_id');
    }
}

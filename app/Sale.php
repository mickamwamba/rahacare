<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    //

    public function order_item()
    {
    	return $this->belongsTo('App\OrderItem','order_item_id');
    }
}

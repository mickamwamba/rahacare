<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    public function method()
    {
    	return $this->hasOne('App\TransactionMethod','id','transaction_method_id');
    }

    public function order()
    {
    	return $this->belongsTo('App\Order','order_id');
    }
}

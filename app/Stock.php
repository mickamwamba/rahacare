<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    //
  	protected $table = 'stock';

  	public function product()
  	{
  		return $this->belongsTo('App\Product','product_id');
  	}
}

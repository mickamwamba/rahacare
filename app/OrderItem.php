<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    //
    protected $table = 'order_items';

    public function order()
    {
    	return $this->belongsTo('App\Order','order_id');
    }

    public function shipment_product()
    {
    	return $this->belongsTo('App\ShipmentProduct');
    }
    public function sale()
    {
        return $this->hasOne('App\Sale');
    }

    public function product_option(){
        return $this->belongsTo('App\ProductOption','product_optioin_id');
    }
}

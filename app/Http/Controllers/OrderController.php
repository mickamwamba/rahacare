<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderItem;
use App\ShipmentProduct;
use App\User;
use App\Region;
use DB;
use Auth;

class OrderController extends Controller
{
    //
        public  function placeOrder(Request $request)
        {
            if($user = User::find($request->user_id))
            {
            try
            {
                DB::transaction(function() use($request){
                    $order = new Order;
                    $order->user_id = $user_id;
                    $order->status = 'PENDING';
                    $order->paid = 0;
                    
                    $order->save();
                    foreach ($request->order_items as $item) {
                        $order_item = new OrderItem;
                        $order_item->product_id = $item->product_id;
                        $order_item->quantity = $item->quantity;
                        $order_item->order_id = $order->id;
                        $order_item->shipment_product_id = ShipmentProduct::where('status','ACTIVE')->first()->id;
                        $order_item->size = $item->size;
                        $order_item->status = 'PENDING';
                        $order_item->product_option_id = $item->product_option_id;
                        
                        $order_item->save();

                    }

                });
            }
            catch(Exception $e)
            {
                return redirect()->back()->with('msg','Error! Failed to place order, please try again');
            }
            return view('/shopping/cart/checkout')->with('msg','Order received successfully');
        }
        else return redirect('signup');
    }

    public function confirmOrder(Request $request)
    {       
        if (!$user = User::find(Auth::user()->id))  return redirect('login');
  
        try
        {
            DB::transaction(function() use($request,$user){
                $order = new Order;
                $order->user_id = $user->id;
                $order->status = 'NEW';
                $order->paid = 0;
                $order->save();

                foreach (json_decode($request->cart) as $cart_item) {
                $order_item = new OrderItem;
                $order_item->product_id = $cart_item->product_id;
                $order_item->quantity = $cart_item->quantity;
                $order_item->product_option_id = $cart_item->product_option_id;                
                $order_item->size = $cart_item->size;
                $order_item->order_id = $order->id;
                $order_item->shipment_product_id = $cart_item->shipment_product_id;
                $order_item->status = 'PENDING';

                $order_item->save();
                }
            });
        }
        catch(Exception $e)
        {
            return redirect()->back();
        }
        $latest_order = $this->getLatestUserOrder($user->id);
        $regions = Region::all();
        if(!$user->region){
            $user->region_id = 2; //set default region dar
            $user->save();
        }
        $delivery_cost = $this->getDeliveryCost($user->region->id);        
       return view('checkout',['user'=>$user,'order'=>$latest_order,'regions'=>$regions,'delivery_cost'=>$delivery_cost]);
    }

    public static function getLatestUserOrder($user_id)
    {
        return Order::orderBy('created_at','DESC')->where('user_id',$user_id)->first();
    }

    public static function getOrderCost($order_id)
	{
		// var_dump($order_id); exit();
		if ($order = Order::with('items.shipment_product')->find($order_id)) 
		{		
			// var_dump($order->items); exit();
			$sum = 0;
				foreach ($order->items as $item) {
					// var_dump($item->shipment_product); exit();
					$price = $item->quantity * ProductController::getLatestProductPrice($item->shipment_product->product_id);
					$sum = $sum +$price;
				}
				return $sum;
		}
		else return false;
    }
    
    public static function getDeliveryCost($region_id)
    {

       if($region = Region::find($region_id)){
        //    var_dump(env('DELIVERY_COST_DAR',false)); exit();
           if($region->name == 'Dar es salaam') return env('DELIVERY_COST_DAR',false);
           else if($region->name=='Outside Tanzania') return env('DELIVERY_COST_INTERNATIONAL',false);
           else return env('DELIVERY_COST_OUT_OF_DAR',false);
       }
       else return false;
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Region;
use Auth;

class UserController extends Controller
{
    //
    public function showRegisterForm()
    {   
        Auth::logout();  //logout current user
        $regions = Region::all();
        return view('auth.signUp',['regions'=>$regions]);
    }
}

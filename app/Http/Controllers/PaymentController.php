<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect; 
use App\Transaction;
use Pesapal;
use App\User;
use App\Order;


class PaymentController extends Controller
{
    
    public function completeOrder(Request $request)
    {
        $transaction = new Transaction;
        $transaction->amount = OrderController::getOrderCost($request->order_id);
        $transaction->order_id = $request->order_id = $request->order_id;
        $transaction->transaction_method_id = 3;
        $transaction->user_id = $request->user_id;
        $transaction->status = ($this->chargeCard($request,$transaction->amount))?'SUCCESSFUL':'FAILED';
        $transaction->save();

        if($transaction->status=='SUCCESSFUL') return redirect('confirm_purchase')->with('msg','Payment received successfully ');
        else redirect()->back()->with('msg','Error! Payment failed, Please try again ');

    }
    public static function chargeCard(Request $request,$amount)
    {
            \Stripe\Stripe::setApiKey('sk_test_WpnB4ne6RXxS7t1HFMPHJWvG');
           
            try {
                \Stripe\Charge::create ( array (
                        "amount" => $amount * 100,
                        "currency" => "tzs",
                        "source" => $request->stripeToken, // obtained with Stripe.js
                        "description" => "Test payment." 
                ) );
                // Session::flash ( 'success-message', 'Payment done successfully !' );
                // return Redirect::back()->with('msg','Payment done successfully');
                return true;
            } catch ( \Stripe\Error\Card $e ) {
                // $body = $e->getJsonBody();
                // $err  = $body['error'];
                // var_dump($err); exit();
                // Session::flash ( 'fail-message', "Error! Please Try again." );
                // var_dump($e); exit();
                // return Redirect::back()->with('msg','Error! Please try again');
                return false;
            }
    }

    public function processWithPesaPal(Request $request)
    {
        $user = User::find($request->user_id);
        // var_dump($user->region_id); exit();
        $user->phone = ($user->phone==NULL)?$request->phone:$user->phone;
        $user->region_id = ($user->region_id==NULL)?$request->region_id:$user->region_id;        
        $user->save();

        $transaction = new Transaction;
        $transaction->amount = OrderController::getOrderCost($request->order_id) + $request->delivery_cost;
        $transaction->order_id = $request->order_id = $request->order_id;
        $transaction->transaction_method_id = 3;
        $transaction->user_id = $user->id;
        $transaction->status ='NEW';   // Initial value before user gets to the iframe
        $transaction->save();
        // $total_cost = $transaction->amount + $request->delivery_cost;
        // var_dump($total_cost); exit();
        $details = array(
            'amount' =>$transaction->amount,
            'description' => 'Order plus Delivery  Payment',
            'type' => 'MERCHANT',
            'first_name' => $user->fname,
            'last_name' => $user->sname,
            'email' => $user->email,
            'phonenumber' => $user->phone,
            'reference' => $transaction->id,
            'height'=>'400px',
            'currency' => 'TZS'
        );

        $iframe= Pesapal::makePayment($details);
        return view('payment', compact('iframe'));


    }

    public function getPesapalResponse(Request $request)
    {
        $transaction_id = $request->merchant_reference;
        $pesapal_transaction_tracking_id = $request->tracking_id;

        $transaction = Transaction::find($transaction_id);
        $transaction->pesapal_transaction_tracking_id = $pesapal_transaction_tracking_id;
        $transaction->status = 'PENDING'; // After receiving response from pesapal, tag this transaction as PENDING instead of NEW
        // $status=Pesapal::getMerchantStatus($transaction_id);  
        // $transaction->status = $status;     
        $transaction->save();
        
        return view('confirm_purchase');
        
    }

    public function transactionConfirmation(Request $request)
    {
        $transaction_id = $request->pesapal_merchant_reference;
        $pesapal_transaction_tracking_id = $request->pesapal_transaction_tracking_id;

        $pesapal_notification_type= $request->pesapal_notification_type;

        //use the above to retrieve payment status now..
        $this->checkTransactionstatus($pesapal_transaction_tracking_id,$transaction_id,$pesapal_notification_type);
    }
    //Confirm status of transaction and update the DB

    public function checkTransactionstatus($pesapal_transaction_tracking_id,$transaction_id,$pesapal_notification_type){
        $status=Pesapal::getMerchantStatus($transaction_id);

        $transaction = Transaction::where('pesapal_transaction_tracking_id',$pesapal_transaction_tracking_id)->first();
        $transaction->status = $status;
        $transaction->test = $status;
        if($status=='COMPLETED'){
            $order = Order::find($transaction->order_id);
            $order->paid = 1;
            $order->status = 'PENDING';
            $order->save();
        }
        // $transaction->payment_method = "PESAPAL";//use the actual method though...
        $transaction->save();
        return "success";
    }

    
}



<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FAQ;

class FAQController extends Controller
{
    //
    public function getFAQs()
    {
        if($faqs = FAQ::all()) return view('faquestions',['faqs'=>$faqs]);
        else return redirect()->back()->with('msg','No FAQ found');
    }
}
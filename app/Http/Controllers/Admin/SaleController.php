<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OrderItem;
use App\Sale;

class SaleController extends Controller
{
    //

    public static function completeSale($order_item_id)
    {
    	if ($order_item = OrderItem::find($order_item_id)) {
    		$sale = new Sale;
    		$sale->order_item_id = $order_item->id;
    		$sale->status = 'COMPLETE';
    		if($sale->save()) return true;
    		else return false;
    	}
    	else return false;
    }

    public static function getCurrentMonthSales()
    {
    	$sales = Sale::with('order_item.shipment_product')->where('status','COMPLETE')->where('created_at','>=',date("Y-m-d H:i:s",strtotime("first day of this month")))->where('created_at','<=',date("Y-m-d H:i:s",strtotime("last day of this month")))->get();
   		if($sales) 
   		{
   			$total_sales = 0;
   			foreach ($sales as $sale) {
   				$total_sales = $total_sales + ($sale->order_item->quantity) * ($sale->order_item->shipment_product->unit_selling_price);
   			}
   			return $total_sales;
   		}
   		else return false;
	}
	
	public static function getTotalSales($sales)
	{
		if($sales) 
		{
			$total_sales = 0;
			$products = 0;
			$revenue = 0;
			foreach ($sales as $sale) {
				$products++;
				$revenue += ($sale->order_item->quantity) * ($sale->order_item->shipment_product->unit_selling_price);
			}
			$total_sales = array('products'=>$products,'revenue'=>$revenue);
			return $total_sales;
		}
		else return false;
	}
}

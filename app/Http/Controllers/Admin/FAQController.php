<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FAQ;

class FAQController extends Controller
{
    //
    public function getFAQs()
    {	$count = 10;
    	if ($faqs = FAQ::orderBy('created_at','DESC')->paginate($count)) return view('admin.faqs',['faqs'=>$faqs]);
 		else return redirect()->back()->with('msg','No FAQ found');
    }

    public function getFAQ($faq_id)
    {
        if($faq = FAQ::find($faq_id)) return view('admin.faq',['faq'=>$faq]);
        else return redirect()->back()->with('msg','FAQ not found');
    }

    public function addFAQ(Request $request)
    {
    	$faq = new FAQ;
    	$faq->question = $request->question;
    	$faq->answer = $request->answer;
    	if ($faq->save()) return redirect()->route('admin.faqs')->with('msg','FAQ Added successfully');
    	else return redirect()->back()->with('msg','Failed to add FAQ');
    }

    public function editFAQ(Request $request)
    {
        if ($faq = FAQ::find($request->faq_id)) 
        {
            $faq->question = $request->question;
            $faq->answer = $request->answer;
            if($faq->save()) return redirect()->back()->with('msg','FAQ Edited successfully');
            else return redirect()->back()->with('msg','Failed to update FAQ');
        }
        else return redirect()->back()->with('msg','FAQ not found'); 
    }

    public function deleteFAQ(Request $request)
    {
        if ($faq = FAQ::find($request->faq_id))
        {
            if($faq->delete()) return redirect()->route('admin.faqs')->with('msg','FAQ Deleted successfully');
            else return redirect()->back()->with('msg','Failed to delete FAQ');
        }
        else return redirect()->back()->with('msg','FAQ not found'); 
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\CustomDate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\OrderItem;
use DB;
use App\Http\Controllers\Admin\SaleContoller;
use App\ProductOption;

class OrderController extends Controller
{
    //
    public function getOrders()
    {
    	if($orders = Order::orderBy('created_at','DESC')->paginate(10)) {
    		foreach ($orders as $order) {
                $order->value = OrderController::getOrderCost($order->id);
    			}
    		return view('admin.orders',['orders'=>$orders]);	
    	}
    	
    	else return redirect()->back()->with('msg','Orders not found');	
    }

    public function getOrder($order_id)
    {	
    	if($order = Order::with('items.shipment_product.product.stock','customer','transactions')->find($order_id)) {
			// var_dump($order->items[0]->product_option_id); exit();
			foreach($order->items as $item){
				if($item->product_option_id!=NULL){
					$op = ProductOption::with('option')->find($item->product_option_id);
					$item->color = $op->option->value;	
					// var_dump($color); exit();
				}
			
			}
			return view('admin.order',['order'=>$order]);
		}
		
		else return redirect()->back()->with('msg','Order not found');
    }

	public static function getOrderCost($order_id)
	{
		// var_dump($order_id); exit();
		if ($order = Order::with('items.shipment_product')->find($order_id)) 
		{		
			// var_dump($order->items); exit();
			$sum = 0;
				foreach ($order->items as $item) {
					// var_dump($item->shipment_product); exit();
					$price = $item->quantity * ProductController::getLatestProductPrice($item->shipment_product->product_id);
					$sum = $sum +$price;
				}
				return $sum;
		}
		else return false;
	}

	public function filterOrders(Request $request)
	{
		// var_dump($request->from_date); exit();
		$query = Order::with('customer');

		if($request->has('customer_id')&&isset($request->customer_id)){
			$query = $query->where('user_id',$request->customer_id);
		}

		if($request->has('from_date')&&isset($request->from_date)){
			$query = $query->whereDate('created_at','>=',date("Y-m-d",strtotime(CustomDate::createMysqlDate($request->from_date))));
			// var_dump($query->get()); exit();
		}
		if ($request->has('to_date')&&isset($request->to_date)) {
			$query = $query->whereDate('created_at','<=',date("Y-m-d",strtotime(CustomDate::createMysqlDate($request->to_date))));

		}

		if ($request->has('status')&&isset($request->status)) {
			
			$query = $query->where('status',$request->status);
		}
		$query = $query->paginate(10);
		// $orders = $query->orderBy('created_at','DESC')->paginate(10);
		foreach ($query as $order) {
                $order->value = OrderController::getOrderCost($order->id);
            }
        if ($request->has('customer_id')&&isset($request->customer_id)) {
        	$customer = User::find($request->customer_id);
        	return view('admin.orders',['orders'=>$query, 'customer'=>$customer,'from'=>$request->from_date,'to'=>$request->to_date,'status'=>$request->status]);
        }
		return view('admin.orders',['orders'=>$query,'from'=>$request->from_date,'to'=>$request->to_date,'status'=>$request->status]);
	}

	 public function dispatchOrderItems(Request $request)
	 {
    	
        $selected = $request->input('items');
    	if ($selected!=NULL) {
    		try {
    			DB::transaction(function() use($selected){
    			for ($i=0; $i < count($selected); $i++) { 
	    			if ($selected[$i]=="undefined") {
	    				continue;
	    			}
	    			$item_id = $selected[$i];
	    			$item = OrderItem::find($item_id);
	    			$item->status = 'DISPATCHED';
	    			$item->save();
                    SaleController::completeSale($item->id);
    			}
    			});
    		
    		} catch (Exception $e) {
    			return redirect()->back()->with('msg','Errors occured while dispatching items');
    		}
    		$this->compeleteOrder($request->order_id);
    		return redirect()->back()->with('msg','Items dispatched successfully');
    	}

    	else return redirect()->back()->with('msg','No order item selected');
    }

    public function compeleteOrder($order_id)
    {
    	if($order = Order::find($order_id))
    	{
    		$order->status = 'PROCESSED';
    		return $order->save()?true:false;
    	}
    	else return false;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\CustomDate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\Order;


class TransactionController extends Controller
{
    //

    public function getTransactions()
    {
    	if($transactions = Transaction::orderBy('created_at','DESC')->paginate(10)) return view('admin.transactions',['transactions'=>$transactions]);
    	 else return redirect()->back()->with('msg','No transaction found');
    }

    public function getTransaction($transaction_id)
    {
    	if ($transaction = Transaction::with('order')->find($transaction_id)) {
    	       $transaction->order->value = OrderController::getOrderCost($transaction->order->id);
    		   return view('admin.transaction',['transaction'=>$transaction]);
    	}
 		else return redirect()->back()->with('msg','Transaction not found');
    }

	public function filterTransactions(Request $request)
	{
		// var_dump($request->from_date); exit();
		$query = Transaction::with('method');

		if($request->has('customer_id')&&isset($request->customer_id)){
			$query = $query->where('user_id',$request->customer_id);
		}

		if($request->has('from_date')&&isset($request->from_date)){
			$query = $query->whereDate('created_at','>=',date("Y-m-d",strtotime(CustomDate::createMysqlDate($request->from_date))));
			// var_dump($query->get()); exit();
		}
		if ($request->has('to_date')&&isset($request->to_date)) {
			$query = $query->whereDate('created_at','<=',date("Y-m-d",strtotime(CustomDate::createMysqlDate($request->to_date))));

		}

		if ($request->has('method')&&isset($request->method)) {
			
			$query = $query->where('transaction_method_id',$request->method);
		}

		$query = $query->paginate(10);
		// $orders = $query->orderBy('created_at','DESC')->paginate(10);
	
        if ($request->has('customer_id')&&isset($request->customer_id)) {
        	$customer = User::find($request->customer_id);
        	return view('admin.transactions',['transactions'=>$query, 'customer'=>$customer,'from'=>$request->from_date,'to'=>$request->to_date,'method'=>$request->method]);
        }
		return view('admin.transactions',['transactions'=>$query,'from'=>$request->from_date,'to'=>$request->to_date,'method'=>$request->method]);
	}

}

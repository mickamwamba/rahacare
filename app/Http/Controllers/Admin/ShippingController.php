<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Shipment;
use App\ShipmentProduct;
use App\OrderItem;
use DB;

class ShippingController extends Controller
{
    //
    public function getShippings()
    {
        if ($shipments = Shipment::with('products')->get()) {
            foreach ($shipments as $shipment) {
                $shipment->cost = $this->getShipmentCost($shipment->id);
            }
            return view('admin.shippings',['shipments'=>$shipments]);
        }
        else return redirect()->with('msg','No shipment history found');
    } 

    public function getShipmentCost($shipment_id)
    {
        if ($shipment = Shipment::with('products')->find($shipment_id)) 
        {
            $sum = 0;
            foreach ($shipment->products as $product) {
                $sum += $product->quantity * $product->unit_buying_price; 
            }    
            return $sum;
        }
        else return false;
    }

    public function newShipping()
    {
    	$products = Product::all();
    	return view('admin.addShipping',['products'=>$products]);
    }															

    public function getShipping($shipment_id)
    {
    	if($shipment= Shipment::with('products')->find($shipment_id)){
    		$products = Product::all();
    		return view('admin.shipping',['shipment'=>$shipment,'products'=>$products]);
    	}
 		else return redirect()->back()->with('msg','Shipment not found');
    }

    public function addShipmentProduct(Request $request)
    {
    	if (!$request->has('shipping_id')) {
    		$shipping = new Shipment;
    		try {
    			DB::transaction(function() use ($request,$shipping){
    				$shipping->save();
    				$ship_prod = new ShipmentProduct;
    				$ship_prod->shipment_id = $shipping->id;
    				$ship_prod->quantity = $request->quantity;
    				$ship_prod->unit_buying_price =$request->unit_buying_price;
    				$ship_prod->unit_selling_price = $request->unit_selling_price;
    				$ship_prod->status = 'DISABLED';
    				$ship_prod->product_id = $request->product_id;

    				$ship_prod->save();
    			});

    		} catch (Exception $e) {
    			return redirect()->back()->with('msg','Failed to add product to shipping');
    		}
    		return redirect()->route('admin.shipping',['id'=>$shipping->id])->with('msg','Product added successfully');
    	}
    	else{
    		if ($shipping = Shipment::find($request->shipping_id)) {
    				if(ShipmentProduct::where('shipment_id',$shipping->id)->where('product_id',$request->product_id)->first()) return redirect()->back()->with('msg','Product already added ealier! Edit original entry instead');
    
    				$ship_prod = new ShipmentProduct;
    				$ship_prod->shipment_id = $shipping->id;
    				$ship_prod->quantity = $request->quantity;
    				$ship_prod->unit_buying_price =$request->unit_buying_price;
    				$ship_prod->unit_selling_price = $request->unit_selling_price;
    				$ship_prod->status = 'DISABLED';
    				$ship_prod->product_id = $request->product_id;
    				$ship_prod->save();
    				return redirect()->back()->with('msg','Product added successfully');
    		}
    		else return redirect()->back()->with('msg','Product not found');
    	}
    }

    public function getShipmentProduct($shipment_product_id)
    {
        if ($product = ShipmentProduct::with('product')->find($shipment_product_id)) {
            return view('admin.shipping_product',['product'=>$product]);
        }
        else return redirect()->back()->with('msg','Shipment product not found');
    }

    public function editShipmentProduct(Request $request)
    {
        if ($ship_prod = ShipmentProduct::find($request->shipment_product_id)) {
            $ship_prod->quantity = $request->quantity;
            $ship_prod->unit_buying_price = $request->unit_buying_price;
            $ship_prod->unit_selling_price = $request->unit_selling_price;
            if($ship_prod->save()) return redirect()->back()->with('msg','Shipment product edited successfully');
            else return redirect()->back()->with('msg','Failed to edit shipment product');
        }
        else return redirect()->back()->with('msg','Shipment product not found');
    }

    public function changeShipmentProductStatus(Request $request)
    {
        if ($ship_prod = ShipmentProduct::find($request->shipment_product_id)) {
            $ship_prod->status = $request->status; 
            if($ship_prod->save()) return redirect()->back()->with('msg','Product status chaged successfully');
            else return redirect()->back()->with('msg','Failed to change product status');
        }
        else return redirect()->back()->with('msg','Shipment product not found');
    }

    public function deleteShipmentProduct(Request $request)
    {
        if ($ship_prod = ShipmentProduct::find($request->shipment_product_id)) {
            if (!OrderItem::where('shipment_product_id',$request->shipment_product_id)->first()) {
                if ($ship_prod->delete()) return redirect()->route('admin.shipping',['id'=>$ship_prod->shipment_id])->with('msg','Shipment Product deleted successfully');          
                else return redirect()->back()->with('msg','Failed to delete shipment product');
            }
            else return redirect()->back()->with('msg','Cant delete product, Already on order records ');
        }
        else return redirect()->back()->with('msg','Shipment product not found');
    }
}

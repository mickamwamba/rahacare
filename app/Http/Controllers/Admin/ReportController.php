<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Transaction; 
use App\User;
use App\Sale;
use App\Libraries\CustomDate;

class ReportController extends Controller
{
    //
	public function getReports($from='',$to='')
	{	$reports = $this->extractReports($from,$to);
		$orderCost = 0;
		foreach ($reports['orders'] as $order) {
			$orderCost += OrderController::getOrderCost($order->id);
		}
		$total_sales = SaleController::getTotalSales($reports['sales']);
		return view('admin.reports',['reports'=>$reports,'orderCost'=>$orderCost,'total_sales'=>$total_sales,'from'=>$from,'to'=>$to]);
	}

    public static function extractReports($from='',$to='')
    {
    	// if($from=='') $from = date("Y-m-d",strtotime("first day of last month"));
    	// if($to=='') $to = date("Y-m-d");

		$from = ($from=='')?date("Y-m-d",strtotime("first day of this month")):date("Y-m-d",strtotime(CustomDate::createMysqlDate($from)));
		$to = ($to=='')?date("Y-m-d"):date("Y-m-d",strtotime(CustomDate::createMysqlDate($to)));
		// var_dump($to); exit();
		$orders = Order::whereBetween('created_at',array($from,$to))->get();
    	$transactions = Transaction::whereBetween('created_at',array($from,$to))->get();
    	$customers = User::whereBetween('created_at',array($from,$to))->get();
    	$sales = Sale::whereBetween('created_at',array($from,$to))->get();

    	$reports = array('orders'=>$orders,'transactions'=>$transactions,'customers'=>$customers,'sales'=>$sales);
    	return $reports;
	}
	public function filterReports(Request $request)
	{
		// var_dump($request->to); exit();
		$from = $request->has('from')?$request->from:'';
		$to = $request->has('to')?$request->to:'';
		return $this->getReports($from,$to);
	}
}

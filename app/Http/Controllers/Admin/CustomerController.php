<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Order;
use App\Transaction;

class CustomerController extends Controller
{
    //
    public function getCustomers()
    {
    	$count = 10; 
    	if($customers = User::paginate($count)) return view('admin.customers',['customers'=>$customers]);
   		else return redirect()->back()->with('msg','No customer found');
    }

    public function getCustomer($customer_id)
    {

    	if($customer = User::with(['orders'=> function ($query){
            $query->orderBy('created_at','DESC')->take(5)->get();
        },'transactions'=> function ($query){
            $query->orderBy('created_at','DESC')->take(5)->get();
        }])->find($customer_id))
        {   
            // var_dump(json_encode($customer->transactions)); exit();
            foreach ($customer->orders as $order) {
                // var_dump(OrderController::getOrderCost($order->id)); exit();
                $order->value = OrderController::getOrderCost($order->id);
            }
            return view('admin.customer',['customer'=>$customer]);
        } 
    	
        else return redirect()->back()->with('msg','Customer not found');
    }
    
    public function searchCustomer(Request $request)
    {	
    	$user_input = $request->user_input;
    	$customers = User::where('fname','LIKE','%'.$user_input.'%')->orWhere('sname','LIKE','%'.$user_input.'%')->orWhere('email','LIKE','%'.$user_input.'%')->orWhere('phone','LIKE','%'.$user_input.'%')->paginate(10);
    	return view('admin.customers',['customers'=>$customers,'user_input'=>$user_input]);
    }

    public function getCustomerOrders($customer_id)
    {
        if ($customer = User::with('orders')->find($customer_id)) {
            foreach ($customer->orders as $order) {
                $order->value = OrderController::getOrderCost($order->id);
            }
            return view('admin.orders',['orders'=>$customer->orders,'customer'=>$customer]);
        }
        else return redirect()->back()->with('msg','Orders not found for this customer');
    }

    public function getCustomerTransactions($customer_id)
    {
        if($customer = User::with('transactions')->find($customer_id)){
            return view('admin.transactions',['transactions'=>$customer->transactions,'customer'=>$customer]);
        }
          else return redirect()->back()->with('msg','Transactions not found for this customer');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class AdminController extends Controller
{
    //
    public function authenticateAdmin(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password,'role_id'=>2])) {
            // Authentication passed...
            return redirect()->intended('/');
        }
        else return redirect()->back();
    }

    public function logout()
    {
        Auth::logout();
        
        return redirect()->route('admin.login');
    }
}

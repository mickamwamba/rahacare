<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Transaction;
use App\User;
use App\OrderItem;

class DashboardController extends Controller
{
    //

    public function getDashboard()
    {
    	$orders = Order::with('customer')->orderBy('created_at','DESC')->take(5)->get();
    	foreach ($orders as $order) {
    		$order->value = OrderController::getOrderCost($order->id);
    	}
    	$transactions = Transaction::with('method')->orderBy('created_at','DESC')->take(5)->get();
    	$open_orders = count(Order::where('status','PENDING')->where('paid',1)->get());
    	$customers = count(User::all());
    	$current_month_sales = SaleController::getCurrentMonthSales();
    	
    	$all_orders = $this->Orders();
    	$today = $this->calculateOrderValue($all_orders['today']);
		$yesterday = $this->calculateOrderValue($all_orders['yesterday']);
		$this_week = $this->calculateOrderValue($all_orders['this_week']);
		$last_week = $this->calculateOrderValue($all_orders['last_week']);
		$this_month = $this->calculateOrderValue($all_orders['this_month']);
		$last_month = $this->calculateOrderValue($all_orders['last_month']);
		$orders_summary = array('today'=>$today,'yesterday'=>$yesterday,'this_week'=>$this_week,'last_week'=>$last_week,'this_month'=>$this_month,'last_month'=>$last_month);
    	return view('admin.dashboard',['orders'=>$orders,'transactions'=>$transactions,'open_orders'=>$open_orders,'customers'=>$customers,'current_month_sales'=>$current_month_sales,'orders_summary'=>$orders_summary]);
   
    }

    public function Orders(){
    	$order_items = OrderItem::all();
    	$today = array();
    		$yesterday = array();
    		$this_week= array();
    		$last_week = array();
    		$this_month = array();
    		$last_month = array();
    		$orders = array();

    		foreach ($order_items as $item) {
    			$order_date = date("Y-m-d",strtotime($item->created_at));

    			if(($order_date) == date("Y-m-d")){
    				$today[] = $item->order_id;
    			}

    			if(($order_date) == date("Y-m-d",strtotime("-1 days"))){
    				$yesterday[] = $item->order_id;
    			}

    			if(($order_date) >= date("Y-m-d",strtotime("monday this week")) && ($order_date) <= date("Y-m-d",strtotime("sunday this week")) ){
    				$this_week[] = $item->order_id;
    			}

    			if((($order_date) >= date("Y-m-d",strtotime("monday last week"))) && (($order_date) <= date("Y-m-d",strtotime("sunday last week"))) ){
    				$last_week[] = $item->order_id;
    			}


    			if((($order_date) >= date("Y-m-d",strtotime("first day of this month"))) && (($order_date) <= date("Y-m-d",strtotime("last day of this month"))) ){
    				$this_month[] = $item->order_id;
    			}

    			if((($order_date) >= date("Y-m-d",strtotime("first day of last month"))) && (($order_date) <= date("Y-m-d",strtotime("last day of last month"))) ){
    				$last_month[] = $item->order_id;
    			}
    		}

    			$today = array_unique($today);
    			$yesterday = array_unique($yesterday);
    			$this_week = array_unique($this_week);
    			$last_week = array_unique($last_week);
    			$this_month = array_unique($this_month);
    			$last_month = array_unique($last_month); 

    			$orders['today'] =Order::whereIn('id',$today)->get();
    			$orders['yesterday'] = Order::whereIn('id',$yesterday)->get();
    			$orders['this_week'] = Order::whereIn('id',$this_week)->get();
    			$orders['last_week'] = Order::whereIn('id',$last_week)->get();
    			$orders['this_month'] = Order::whereIn('id',$this_month)->get();
    			$orders['last_month'] = Order::whereIn('id',$last_month)->get();

    			return $orders;
    }

     public function calculateOrderValue($orders)
    {
    	$total = 0; 
    	foreach ($orders as $order) {
    		$total +=  OrderController::getOrderCost($order->id);
    	}
    	return $total;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stock;

class StockController extends Controller
{
    //
    public static function getAvailableStock($product_id)
    {
    	if($stock = Stock::where('product_id',$product_id)->first()) return $stock; 
    	else return false;
    }
}

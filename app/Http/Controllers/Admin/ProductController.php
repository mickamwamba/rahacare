<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\FileManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductMedia;
use App\ShipmentProduct;
use App\Category;
use App\Stock;
use App\Order;
use DB;

class ProductController extends Controller
{
    //
    public function getProducts()
    {
    	if($products = Product::with('media','category')->paginate(10))
    	 {
    	 	$categories = Category::all();
    		return view('admin.products',['products'=>$products,'categories'=>$categories]);
    	 }
    	else return redirect()->back()->with('msg','No product found');
    }

    public function getProduct($product_id)
    {
    	if($product = Product::with('stock','category','order_items','shipment_products')->find($product_id)){
    		// var_dump($product->shipment_products); exit();
    		$orders = $this->getProductOrders($product);
    		$today = $this->calculateProductOrderValue($orders['today']);
    		$yesterday = $this->calculateProductOrderValue($orders['yesterday']);
    		$this_week = $this->calculateProductOrderValue($orders['this_week']);
    		$last_week = $this->calculateProductOrderValue($orders['last_week']);
    		$this_month = $this->calculateProductOrderValue($orders['this_month']);
    		$last_month = $this->calculateProductOrderValue($orders['last_month']);
    		$count = array('today'=>count($orders['today']),'yesterday'=>count($orders['yesterday']),'this_week'=>count($orders['this_week']),'last_week'=>count($orders['last_week']),'this_month'=>count($orders['this_month']),'last_month'=>count($orders['last_month']));
    		$orders_summary = array('today'=>$today,'yesterday'=>$yesterday,'this_week'=>$this_week,'last_week'=>$last_week,'this_month'=>$this_month,'last_month'=>$last_month);
    		$categories = Category::all();
    		return view('admin.product',['product'=>$product,'orders_summary'=>$orders_summary,'order_count'=>$count,'categories'=>$categories]);
  	    	}

    		else return redirect()->back()->with('msg','Product not found');
    }

    public function addProduct(Request $request)
    {
    	$product = new Product;
    	$product->name = $request->name;
    	$product->category_id = $request->category;
    	try {
    		DB::transaction(function() use($product,$request){
    			$product->save();
    			$pm= new ProductMedia;
    			$pm->media_type = 'IMAGE';
    			$pm->media = FileManager::upload($request,'image','/images/products');
    			$pm->product_id = $product->id;
    			$pm->save();

    			$stock = new Stock;
    			$stock->product_id = $product->id; 
    			$stock->available= 0; 
    			$stock->last_update = date("Y-m-d H:i:s");
    			$stock->save();
    		});
    	} catch (Exception $e) {
    		return redirect()->back()->with('msg','Failed to add new product');
    	}

    	return redirect()->back()->with('msg','New product added successfully');
    }


    public function editProduct(Request $request)
    {
    	if ($product = Product::with('media')->find($request->product_id)) {
    		$product->name = $request->name;
    		$product->category_id = $request->category;
    		if($request->hasFile('image')){
    			try {
    				DB::transaction(function() use($product,$request){
    				$product->save();
    				$product->media[0]->media = FileManager::upload($request,'image','/images/products');
    				$product->media[0]->save();
    			});
    			} catch (Exception $e) {
    				return redirect()->back()->with('msg','Failed to edit product');
    			}
    				return redirect()->back()->with('msg','Product edited successfully');
    		}
    		else {
	   			$product->save();
   				return redirect()->back()->with('msg','Product edited successfully');
    		}
    	}
    	else return redirect()->back()->with('msg','Product not found');
    }

    public static function getLatestProductPrice($product_id){

    	if($pr = ShipmentProduct::where('product_id',$product_id)->where('status','ACTIVE')->first()) return $pr->unit_selling_price;
    	else return false;
    }

    public static function getProductOrders($product)
    {
    		$today = array();
    		$yesterday = array();
    		$this_week= array();
    		$last_week = array();
    		$this_month = array();
    		$last_month = array();
    		$orders = array();

    		foreach ($product->order_items as $item) {
    			$order_date = date("Y-m-d",strtotime($item->created_at));

    			if(($order_date) == date("Y-m-d")){
    				$today[] = $item->order_id;
    			}

    			if(($order_date) == date("Y-m-d",strtotime("-1 days"))){
    				$yesterday[] = $item->order_id;
    			}

    			if(($order_date) >= date("Y-m-d",strtotime("monday this week")) && ($order_date) <= date("Y-m-d",strtotime("sunday this week")) ){
    				$this_week[] = $item->order_id;
    			}

    			if((($order_date) >= date("Y-m-d",strtotime("monday last week"))) && (($order_date) <= date("Y-m-d",strtotime("sunday last week"))) ){
    				$last_week[] = $item->order_id;
    			}


    			if((($order_date) >= date("Y-m-d",strtotime("first day of this month"))) && (($order_date) <= date("Y-m-d",strtotime("last day of this month"))) ){
    				$this_month[] = $item->order_id;
    			}

    			if((($order_date) >= date("Y-m-d",strtotime("first day of last month"))) && (($order_date) <= date("Y-m-d",strtotime("last day of last month"))) ){
    				$last_month[] = $item->order_id;
    			}
    		}

    			$today = array_unique($today);
    			$yesterday = array_unique($yesterday);
    			$this_week = array_unique($this_week);
    			$last_week = array_unique($last_week);
    			$this_month = array_unique($this_month);
    			$last_month = array_unique($last_month); 

    			$orders['today'] =Order::whereIn('id',$today)->get();
    			$orders['yesterday'] = Order::whereIn('id',$yesterday)->get();
    			$orders['this_week'] = Order::whereIn('id',$this_week)->get();
    			$orders['last_week'] = Order::whereIn('id',$last_week)->get();
    			$orders['this_month'] = Order::whereIn('id',$this_month)->get();
    			$orders['last_month'] = Order::whereIn('id',$last_month)->get();

    			return $orders;
    }

    public function calculateProductOrderValue($orders)
    {
    	$total = 0; 
    	foreach ($orders as $order) {
    		$total +=  OrderController::getOrderCost($order->id);
    	}
    	return $total;
    }
}

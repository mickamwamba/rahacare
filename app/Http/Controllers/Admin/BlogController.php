<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\FileManager; 
use App\Libraries\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;

class BlogController extends Controller
{
    //
    public function getBlogPosts()
    {
    	$count = 10;
    	if($posts = Blog::orderBy('created_at','DESC')->paginate($count)) return view('admin.blog',['posts'=>$posts]);
    	else return redirect()->back()->with('msg','No Blog post found');
    }

    public function addPost(Request $request)
    {
    	$post = new Blog;
    	$post->title = $request->title;
    	$post->content = $request->content;
    	$post->image = FileManager::upload($request,'image',Resource::blogImagesPath());
  		if($post->save()) return redirect()->route('admin.blog')->with('msg','Blog post added successfully');
  		else return redirect()->back()->with('msg','Failed to add blog post');
    }

    public function getBlogPost($post_id)
    {
    	if($post = Blog::find($post_id)) return view('admin.post',['post'=>$post]);
    	else return redirect()->back()->with('msg','Blog post not found');
    }

    public function editPost(Request $request)
    {
    	if($post = Blog::find($request->post_id))
    	{
    		$post->title = $request->title;
			$post->content = $request->content;
			// var_dump(strip_tags($request->content)); exit();
    		$post->image = $request->hasFile('image')?FileManager::upload($request,'image',Resource::blogImagesPath()):$post->image;
    		if($post->save()) return redirect()->back()->with('msg','Blog post edited successfully');
    		else return redirect()->back()->with('msg','Failed to edit blog post');
    	}
    	else return redirect()->back()->with('msg','Blog post not found');
    }

    public function deletePost(Request $request)
    {
    	if($post = Blog::find($request->post_id))
    	{
    		if($post->delete()) return redirect('/admin/blog')->with('msg','Blog post deleted successfully');
    		else return redirect()->back()->with('msg','Failed to delete post');
    	}

    }
}

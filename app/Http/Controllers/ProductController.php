<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Option;
use App\ShipmentProduct;

class ProductController extends Controller
{
    //

    public function getProducts()
    {
        // var_dump(Product::get()[4]->shipment_products); exit();
        if ($all_products = Product::with('media','shipment_products')->get()) {
            $products = array();
            foreach($all_products as $product)
            {
                // var_dump($product->shipment_products()[0]); exit();
                foreach($product->shipment_products as $x){
                    // var_dump($x['pivot']->status); exit();
                    if($x['pivot']->status=='ACTIVE') $products[] = $product;
                }
                //     if($product->shipment_products()->count()>0) {
            //     $products[] = $product;
            // }

        }
            // var_dump($products[1]->shipment_products[0]->pivot->status); exit();
            return view('products',['products'=>$products]);
            
        }
        else return redirect()->back();
    }

    public function getProduct($product_id)
    {
        if ($product = Product::with(['shipment_products' => function ($query) {
            $query->where('status','ACTIVE')->first();
        }],'options.option','category')->find($product_id)) {
            // var_dump($product->category); exit();
            return view('particular_product',['product'=>$product]);}
        else return redirect()->back();
    }

    public static function getLatestProductPrice($product_id){
        
                if($pr = ShipmentProduct::where('product_id',$product_id)->where('status','ACTIVE')->first()) return $pr->unit_selling_price;
                else return false;
            }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller
{
    //
    public function getBlogPosts()
    {   $count = 10;
        if($posts = Blog::orderBy('created_at','DESC')->paginate($count)) return view('blog',['posts'=>$posts]);
        else return redirect()->back()->with('msg','No blog post found');
    }

    public function getBlogPost($post_id)
    {
        if ($post = Blog::find($post_id)) return view('particular_article',['post'=>$post]);
        else return redirect()->back()->with('msg','Post not found');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    public function shipment_products(){
        // return $this->hasMany('App\ShipmentProduct','product_id');
    	return $this->belongsToMany('App\Shipment','shipment_products')->withPivot('quantity','unit_selling_price','unit_buying_price','status','id')->wherePivot('status','ACTIVE');
        
    }

    public function order_items()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function stock()
    {
    	return $this->hasOne('App\Stock');
    }

    public function media()
    {
    	return $this->hasMany('App\ProductMedia');
    }

    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }
    public function options()
    {
        return $this->hasMany('App\ProductOption');
    }
}

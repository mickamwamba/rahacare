<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentProduct extends Model
{
    //
    public function shipment()
    {
    	return $this->belongsTo('shipment','shipment_id');
    }

    public function product()
    {
    	return $this->belongsTo('App\Product','product_id');
    }

    public function order_item()
    {
    	return $this->hasMany('App\OrderItem');
    }
}

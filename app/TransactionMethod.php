<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionMethod extends Model
{
    //
    
    protected $table = 'transaction_methods';
}

<?php


namespace App\Social;

use App\User;
use Laravel\Socialite\Facades\Socialite;

class GoogleServiceProvider extends AbstractServiceProvider{

/**
     *  Handle Facebook response
     * 
     *  @return Illuminate\Http\Response
     */
    public function handle()
    {   
       $user = $this->provider->user();
        $existingUser = User::where('settings->google_id', $user->id)->first();

        if ($existingUser) {
            $settings = $existingUser->settings;

            if (! isset($settings['google_id'])) {
                $settings['google_id'] = $user->id;
                $existingUser->settings = $settings;
                $existingUser->save();
            }

            return $this->login($existingUser);
        }
       
        $newUser = $this->register([
            'fname' => $user->user['name']['givenName'],
            'sname' => $user->user['name']['familyName'],
            'email' => $user->email,
            'role_id'=> 1,
            'settings' => [
                'google_id' => $user->id,                
            ]
        ]);        

        return $this->login($newUser);
    }     

}
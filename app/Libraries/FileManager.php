<?php
namespace App\Libraries;
/**
* 
*/
class FileManager
{
	
	public static function upload($request,$file_name,$destinationPath)
    {
        $file = substr($request->file($file_name)->getClientOriginalName(),0,-4);
        $fileName = $file.time().".".$request->file($file_name)->getClientOriginalExtension();
        $request->file($file_name)->move(public_path().$destinationPath, $fileName);
        return $destinationPath."/".$fileName;
    }
}

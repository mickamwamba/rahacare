<?php
/**
* 
*/
namespace App\Libraries;

class Resource
{	
	const ROOT_PATH="/";
	const DEFAULT_PATH_CSS="css/";
	const DEFAULT_PATH_JS="js/";
	const DEFAULT_PATH_IMAGES="images/";
	const DEFAULT_PATH_BLOG_IMAGES="images/blog/";
	const DEFAULT_PATH_ICONS="images/icons/";
	
	public static function imageSRC($filename){
		return self::ROOT_PATH.self::DEFAULT_PATH_IMAGES.$filename;
	}

	public static function blogImagesPath()
	{
		return self::ROOT_PATH.self::DEFAULT_PATH_BLOG_IMAGES;
	}
}
<?php

namespace App\Libraries;
/**
* 
*/
class CustomDate{
	
	public static function createMysqlDate($date){
		$date = explode(" ", $date);
		$date_parts = explode("/", $date[0]);
		$day = $date_parts[0];
		$month = $date_parts[1];
		$year = $date_parts[2];
		$time = $date[1];

		$date_str = $year."-".$month."-".$day." ".$time;
		$date = date("Y-m-d H:i:s", strtotime($date_str));
		return $date;
	}
}
?>
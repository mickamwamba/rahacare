//payment tabs script
function openTab(evt, stepName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(stepName).style.display = "block";
    evt.currentTarget.className += " active";
}

//item increment script
var current_items = document.getElementById('current_item_no').value;

$('#increase-item').click(function () {
    $('#current_item_no').val(++current_items);
});
$('#decrease-item').click(function () {
    $('#current_item_no').val(--current_items);
});


//read more text from faq
function changeSigns(id, size) {
    var i;
    for(i=1; i<=size; i++){
        $('.image-read-faq-'.concat(i)).text(function () {
            return '+';
        });
    }
    $('.'.concat(id)).text(function (i, old) {
        return old == '+' ? '-' : '+';
    });
}

//read more text on about us
function readMoreAbout() {
    if ($('#txt-read-more').text() == 'READ MORE') {
        $('#txt-read-more').text("READ LESS");
        document.querySelector("#aboutUs").classList.remove("home-about-us");
        document.querySelector("#aboutUs").classList.add("home-about-us-long");
        document.querySelector("#arrow-read-about").classList.remove("fa-angle-down");
        document.querySelector("#arrow-read-about").classList.add("fa-angle-up");
    } else {
        $('#txt-read-more').text('READ MORE');
        document.querySelector("#aboutUs").classList.add("home-about-us");
        document.querySelector("#aboutUs").classList.remove("home-about-us-long");
        document.querySelector("#arrow-read-about").classList.remove("fa-angle-up");
        document.querySelector("#arrow-read-about").classList.add("fa-angle-down");
    }
}


/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "80%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

//script for auto scroll effect
$('a[href^="#"]').click(function (e) {
    var target = $(this).attr('href');
    var strip = target.slice(1);
    var anchor = $("a[name='" + strip + "']");

    e.preventDefault();

    $('html, body').animate({
        scrollTop: anchor.offset().top
    }, 'slow');
});